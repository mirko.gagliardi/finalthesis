\section{Directory Controller}

nu+ many-core supports a sparse directory coherence mechanism, the L2 cache is an \textit{n}-ways set-associative memory strictly inclusive. The L2 is equally distributed over the tiles, each equipped with a directory controller which is the default home node for the given L2 subset. The \texttt{Directory Controller} handles coherence requests from cache controllers, tracks all sharers node in case block line in \texttt{S} state, and manages the writing back of a memory line into the main memory in case of block eviction or explicit recall.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Cohe/figures/DC.eps}
	\caption{Directory Controller overview.}
	\label{fig:dc}
\end{figure}

Figure \ref{fig:dc} shows an high level overview of this component. As the Cache Controller, this module tracks pending requests on the transaction status handler register (TSHR). Furthermore, the Directory Controller design is similar to the cache controller one, featuring a protocol ROM to be highly protocol-independent. This component interfaces the Network Interface both on the forward and response output ports, while it is connected to the request and the response port in input. The remainder of this section details internal stages of this component. 

\subsection{Stage 1}
The first stage schedules a pending request from the network. Note that, this component receives request only from the NI, even the local core communicate with it through the common network interface. The first stage allocates the tag and coherence state cache, both updated by the last stage, but read in this one. A fixed priority arbiter checks the schedulability of each pending request on the basis of the current state of the requesting line, the coherence operation on it, and the protocol loaded in the ROM. The writing back requests have the highest priority, followed by the responses and finally coherence requests. The scheduled request is passed to the second stage along with tags, block state, and the TSHR entry (if any). 

Scheduling a request message might allocates resources, hence in order to issue such a requests the arbiter checks the following conditions:

\begin{enumerate}
	\item TSHR is not full.
	\item The network interface is available on the forwarding channel, a request might require to send a forwarded message.
	\item The next stage is not busy. 
\end{enumerate}

\subsection{TSHR Signals}
The Transaction Status Handling Register tracks cache block information whose coherence transactions are pending. When a line is tracked in the TSHR its state is both non-stable and the up-to-date. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.6\textwidth]{Chapters/Cohe/figures/TSHR_entry.png}
	\caption{TSHR entry.}
	\label{fig:tshr}
\end{figure}

A TSHR entry is organized as follows:
\begin{itemize}
	\item Valid: the entry is valid.
	\item Address: block line address.
	\item State: actual coherence state.
	\item Sharers list: list of block sharers (one-hot codified).
	\item Owner: ID of the current block owner if the directory is not.
\end{itemize}

\subsection{Stage 2}
The second stage implements the miss/hit logic, and in case of replacement the pseudo LRU mechanism. It also allocates the L2 data cache, which is organized as a SRAM with a single read and a single write ports. Finally, this stage propagates the read data along with the hit/miss result to the next one. The scheduled request and TSHR information from stage 1 are propagated as part of the output as well.

\subsection{Stage 3}
The last stage processes the incoming request in compliance with the current coherence protocol, taking action on the basis of the protocol ROM outputs, generating forwarded messages and updating both the TSHR and the L2 cache if required.

\subsection{Protocol ROM}

This module stores the current coherence protocol loaded in the directory controller. Figure \ref{fig:msi_dc} shows the default version used in this dissertation. The organization of such table is the same of the cache controller one in section \ref{sec:cc_rom}. The baseline coherence protocol, on the directory side, has been augmented with a new state \texttt{N}, which indicates the block is not cached in the L2 and has to be fetched from the main memory. Such a state has been necessary for a main reason: the implemented L2 has a finite size, it cannot track every memory line, while the stable state \texttt{I} tracks that the block is cached only in the L2 which is the most up-to-date version. Two non-stable states have been added in order to fully support the additional state:

\begin{enumerate}
	\item State \texttt{MN\_A}: issued after a replacement in the L2 of a block in state \texttt{M}. The directory controller is waiting for data from owner in order to write it back into the main memory. Further requests on the same block are stalled until data has been received from owner and sent to the main memory. 
	\item State \texttt{NS\_D}: issued after a load request for a non-cached block. The directory controller is waiting for data coming from the main memory. Further requests on the same block are stalled until data has been fetched and sent back to requestor(s).
\end{enumerate}

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Cohe/figures/MSI_DC.jpg}
	\caption{Default MSI protocol on the directory side.}
	\label{fig:msi_dc}
\end{figure}