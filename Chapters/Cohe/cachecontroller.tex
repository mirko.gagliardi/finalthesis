\section{Cache Controller}

The \texttt{Cache Controller} handles coherence on the core side, manages the L1 data and info caches in the Load/Store unit, and dispatches coherence transactions over the network when required. 

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Cohe/figures/CC.eps}
	\caption{Cache Controller overview.}
	\label{fig:cc}
\end{figure}

Pending requests from the core are store in the \texttt{Core Interface} module which has separate FIFOs for each type of request, namely load, store, and eviction. Dually, the \texttt{Network Interface} module (described in section \ref{sec:NI}) provides an interfacing port for each type of incoming transaction from the network, such as forwarded, and response coherence messages. 
The component is composed of 4 stages, summarized as follow:

\begin{itemize}
	\item Stage 1: schedules a valid pending requests, it can be from the local core or from the network.
	\item Stage 2: stores coherence status cache and manages the miss status holding registers (MSHR).
	\item Stage 3: processes a request in compliance with loaded coherence protocol.
	\item Stage 4: prepares coherence transactions to flow over the network.
\end{itemize}

An overview of the Cache Controller and its stages are depicted in the Figure \ref{fig:cc}. 
The component has been realized in a pipelined fashion in order to serve multiple requests at the same time with a reduced latency. The design of the presented Cache Controller has been driven by the following assumptions, that eased the component implementation:

\begin{enumerate}
	\item Transactions involve only memory blocks.
	\item Only requests from the local core (load, store, replacement, flush) can allocate MSHR entries.
	\item Info regarding cache blocks in non-stable state are stored in MSHR otherwise in L1 cache.
	\item Two requests on the same block cannot be issued in a pipelined mode, in such cases the first might modify the MSHR entry after two clock cycles, while the second request may read a not up-to-date entry.
\end{enumerate}

\subsection{Stage 1}
Memory requests either come from the core (due load/store misses) or the network (such  as forward and response coherence requests), the first stage arbiters all these pending requests. It first checks which is schedulable by checking the protocol ROM, which stores the adopted coherence protocol details. The protocol might stall any request, and, of course, this decision is made on the base of the requesting memory block state. E.g. the core issued a store miss request for block in the \texttt{S} state, allocating an MSHR entry which states that this block is in state \texttt{SMa} waiting for all ACKs from other sharers. While in this state, further load/store requests to this block are stalled until the block transits in stable state.

Defined all the eligible requests, a fixed priority arbiter selects one of them and forwards it to the next stage the selected request. The arbiter has a fixed priority in order to match coherence needs, such as response messages are always scheduled when pending. The selected request is forwarded to the snooping bus (detailed in the next subsection) and to the next stage.

\subsection{Stage 2}
As said above, the Cache Controller has a privileged bus through both tags and data caches in the Load/Store unit (referred as snooping bus from now onward). Such a bus eases the task of managing caches, it also allows to check if a scheduled request has been already satisfied from previous transactions. When a memory request is issued from the Load/Store unit to the Cache Controller, it is stored in the interface FIFO along with the other pending requests. Being multi-threaded, the core might issue several requests to the same memory block. The Cache Controller checks through the snooping bus if the missing conditions are still true before processing the request itself. 

This stage gathers all the needed information to process the current request, fetching the non-stable state from the MSHR if the request refers to a block already pending in the miss status holding registers, or fetching the stable state from the \texttt{Coherence State} SRAM allocated if the MSHR has no entry regarding that memory block. An MSHR entry is organized as follow:

\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Cohe/figures/MSHR_entry.png}
	\caption{MSHR entry overview.}
	\label{fig:MSHR_entry}
\end{figure}

\begin{itemize}
	\item Valid: states the validity of the entry.
	\item Address: pending request memory address.
	\item Thread ID: requesting thread ID.
	\item Wakeup Thread: wakes the thread up when transaction is over.
	\item State: actual line non-stable state.
	\item Waiting for eviction: asserted for replacement requests.
	\item Ack count: remaining ACKs to receive from sharers, when needed.
	\item Data: data associated to request. Note that entry's Data part is stored in a separate memory in order to ease the lookup process.
\end{itemize}

If a block is pending in the MSHR, its current state is retrieved from the MSHR which holds the most up-to-date state. All the information along with the corresponding MSHR entry (if any) and data are passed to the next step.

\subsection{Stage 3}
The third stage processes the current request in compliance with the protocol ROM, which states for the current request and state the actions to take. The Cache Controller design has kept been as generic as possible, in fact the protocol ROM outputs atomic actions, such as allocate/deallocate/update the MSHR, update caches in the Load/Store unit, send a message over the network, or start a replacement operation.

Caches maintenance is accomplished using commands described in Section \ref{sec_ldst} through a dedicated bus which injects those commands in the second stage of the Load/Store unit.

Furthermore, this stage handles the MSHR allocated in the second stage. An entry is allocated when a cache line state turns into a non-stable state, meaning that the Cache Controller cannot fulfil the request itself and forwards it to the Directory Controller, waiting for the response. Dually, a MSHR entry is deallocated a pending line state backs to stable state, usually when the corresponding response is received; meaning that the block is stable in the data cache and no further action are required. Finally, a MSHR entry is updated whenever a pending line shifts from its non-stable state to another non-stable one. This is the case of \texttt{MIa}$\rightarrow$\texttt{SIa}. The pending line was in \texttt{MIa} state after an explicit \texttt{putM} request, while a \texttt{fwd-getS} is scheduled. Each condition is represented by a signal that is properly asserted by protocol ROM. 

\subsection{Stage 4}
The last stage provides an interface with the network and builds the coherence transaction which will flow over the network-on-chip. When a request requires info or data from another coherence actor, this stage bridges the Cache Controller with the Network Interface module. The type of the coherence message, the info, and the message destination are specified by the protocol ROM. E.g. after a store miss on non-cached block, the last stage builds a \texttt{getM} packet and forwards it over the request virtual channel.  

\subsection{Protocol ROM} \label{sec:cc_rom}

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Cohe/figures/MSI_CC.jpg}
	\caption{Default MSI protocol implemented at the Cache Controller level.}
	\label{fig:pROM}
\end{figure}

This module stores the current coherence protocol adopted. Figure \ref{fig:pROM} shows the baseline MSI adopted in this dissertation. The choice to implement the protocol as a separate ROM has been made to ease further optimizations or changes to the baseline protocol. It takes in input the current state and the request type and outputs next actions.
The coherence protocol detailed in Figure \ref{fig:pROM} is a standard MSI adapted to the inclusive L2. In particular a new type of forwarded request has been added, namely \texttt{recall}, sent by directory controllers when a block has to be evicted from an L2 cache. A writeback response to the memory controller follows in response to a recall only when the block is in state \texttt{M}. %Note that a writeback response is sent to the directory controller as well in order to provide a sort of acknowledgement.

%The coherence subsystem has been augmented with another type of request, flush (not present in figure), has been added that simply send updated data to the memory. It also generates a writeback response even though it is directed only to memory controller and doesn't change its coherence block state while a writeback response to a recall invalidates that block.


