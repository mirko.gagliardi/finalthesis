\chapter{Technical Background} \label{chapt:back}
%
%
\vskip 1cm
%
%
During the last years Field Programmable Gate Arrays and Graphics Processing Units have become increasingly important for high-performance computing. In particular, a number of industrial solutions and academic projects are proposing design frameworks based on FPGA-implemented GPU-like compute units.

This Chapter presents an overview of parallelism in CPUs, GPUs and GPU-like architectures, network-on-chip, cache coherence issue in distributed architectures, along with related work and existing techniques. The scope and the amount of related work is large, so we focus on the aspects most fundamental and related to the research in this dissertation. Section \ref{sec:het} presents background on heterogeneous computing in HPC and develops modern problem. Sections \ref{sec:ilp} and \ref{sec:gpu} present parallel techniques and architectures, raging from vector processors to GPUs. Section \ref{sec:custacc} considers future trends for many-core and custom accelerators. Section \ref{sec:cohe} develops coherence techniques developed for prior NoCs many-core systems. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   Parallelism in CPUs
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{Chapters/Background/cpu.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   GPU-like Paradigms
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\input{Chapters/Background/gpu.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   Heterogenous Computing
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[t]
    \centering
    \includegraphics[width=1\textwidth]{Chapters/Background/Figures/heter}
    \caption{Shifting from multi-core to heterogeneous systems.}
    \label{fig:hetsys}
\end{figure}

\section{Heterogeneous Computing in HPC} \label{sec:het}
Current trends in HPC are increasingly moving towards heterogeneous platforms, i.e. systems made of different computational units, with specialized accelerators that complement general purpose CPUs, including DSPs or graphics processing units, co-processors, and custom acceleration logic (as shown in Figure  \ref{fig:hetsys}), enabling significant benefits in terms of both power and performance.
%
Historically, HPC has never extensively relied on FPGAs, mostly because of the reduced support for floating-point arithmetic, as FPGAs are tipically fixed-point oriented. Furthermore, designing an FPGA-based hardware accelerator is challenging at the programmability level, as it requires the developer to be a highly-skilled hardware designer knowing low-level hardware description languages such as VHDL or Verilog. Consequently, most high-performance architectures only use custom components for very specific purposes, if any, while they mostly rely on general-purpose compute units such as CPUs and/or GPUs, which deliver adequate performance while ensuring programmability and application portability. However, unfortunately, pure general-purpose hardware is affected by inherently limited power-efficiency, that is, low GFLOPS-per-Watt, now considered as a primary metric.
%
%%%%%%%%%%%%%%%%%%%%%%%
%%FPGA in HPC
%%%%%%%%%%%%%%%%%%%%%%%
%
%%
The other historical problem with FPGAs is programmability. Designing a complex architecture on FPGA, as mentioned above, requires a highly-skilled hardware designer. To overcome this limitation, Altera and Xilinx are bringing the GPU programming model to the FPGA domain.
%%
The Altera SDK for OpenCL~\cite{altera-opencl} makes FPGAs accessible to non-expert users. This toolkit allows a user to abstract away the traditional hardware FPGA development flow, effectively creating custom hardware on FPGAs for each instruction being accelerated. Altera claims that this SDK provides much more power-efficient use of the hardware than a traditional CPU or GPU architecture.
%%
On other hand, similar to the Altera SDK for OpenCL, Xilinx SDAccel~\cite{sdaccel}, enables traditional CPU and GPU developers to easily migrate their applications to FPGAs while maintaining and reusing their OpenCL, C, and C++ code.
%%
Driven by these innovations, FPGAs are becoming increasingly attractive for HPC applications, offering a fine grained parallelism and low power consumption compared to other accelerators.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   Custom accelerators
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Open-source and FPGA-based Accelerators} \label{sec:custacc}
Well-known RISC softcore processors Xilinx's MicroBlaze \cite{xilinx2006microblaze} and Altera's Nios II \cite{nios2009processor} are widely used, providing efficient sequential architectures, optimized for the reconfigurable devices of their respective designers. Both softcores come along with a software development toolchain with an extensive library base for fast application development, both based on the GNU tools. However, these processors do expose a small degree of customizability, although the largest part of the design is fixed. Moreover, they are not open source and in many situations require costly licenses to be used.

For these reasons, academic and industrial research is focusing on GPU-like paradigms to introduce some form of programmability in FPGA design. In the last years, a few GPU-like projects have appeared. Many of them targets FPGA, since they represent the most suitable evaluation platform, and recently a valid concurrent to ASIC GPUs. Recently Altera and Xilinx, the two prominent FPGA manufacturers, focused on overcoming FPGA floating-point limitations. In particular, Altera, now part of Intel Corporation, has developed a new floating-point technology (called Fused Datapath) and toolkit (DSP Builder) intended to achieve maximum performance in  floating-point design implementing on Altera FPGAs~\cite{altera}. As matter of facts, Altera new Stratix 10 series claims to achieve up to $10$ tera floating point operations per second (TFLOPS) of single-precision floating-point performance making these devices the highest performance DSP devices with a fraction of the power of alternative solutions like GPUs \cite{stratix10wp}.

On the other hand, Xilinx is putting the same effort on the 7-Series and Ultrascale FPGAs and All Programmable SoCs are highly power efficient and high-performance oriented. The parallelism and customizable architecture inherent in the FPGA architecture is ideal for high-throughput processing and software acceleration. With the UltraScale family of FPGAs, Xilinx continues to provide customers the best performance-per-watt solutions in the market, enabling design performance goals to be met within the power budget of the application \cite{leibson2016xilinx}. In many real appication, Ultrascale results in up to $25\%$ lower power consumption than the competing $20$nm FPGA.

Microsoft researchers are working on advancing cloud technologies, named Catapult, and are using the Arria 10 FPGAs that ideally has 1 TFLOPs, and up to ideal 40 GFLOPS-per-Watt \cite{ovtcharov2015accelerating}. Actually Catapult equips a Stratix V, that consumes no more than 25W of power, meanwhile the GPGPU solutions require up to 235W of power to operate for the same workload.

Kingyens and Steffan~\cite{kingyens} propose a softcore architecture inspired by graphics processing units mostly oriented to FPGAs. The architecture supports multithreading, vector operations, and can handle up to $256$ concurrent thread.

Nyami/Nyuzi \cite{nyuzi} is a GPU-like RISC architecture inspired by Intel Larrabee. The Nyami HDL code is fully parameterizable and it provides a flexible framework for exploring architectural tradeoffs. The Nyami project provides a LLVM-based C/C++ compiler and can be synthesized on FPGA.

Guppy \cite{guppy} (GPU-like cUstomizable Parallel Processor prototYpe) is based on the parameterizable soft core LEON3. Guppy main feature is to supports CUDA-like threads in a lock-step manner to emulate the CUDA execution model. 

FlexGrip \cite{andryc2013flexgrip} is a soft-core directly inspired by the NVIDIA G80 architecture. The architecture is described in VHDL and targets a Xilinx Virtex 6. This soft-core completely supports CUDA, it is able to run many application compiled with the \textbf{nvcc} compiler. The main core is strictly coupled with a Xilinx MicroBlaze which handles host communication and the soft-core initialization.

The University of Wisconsin-Madison MIAOW \cite{miaow} (Many-core Integrated Accelerator Of Wisconsin) developed an open-source RTL implementation of the AMD Southern Islands GPGPU ISA. MIAOW main goal is to be flexible and to support OpenCL-based applications, and how control flow optimization can impact GPU-like accelerators performance. The system relies on a host CPU, which configures this GPU-like core and also dynamically manages shared resource, assigning them to different threads workgroup.    

The Maven Vector-Thread Architecture \cite{lee2011exploring}, developed by Berkeley University, is a vector-SIMD microarchitecture, which posed the basis for their next architecture called Hwacha \cite{lee201445nm}. This soft-core is an implementation of the RISC-V ISA, an open-source instruction set based on the RISC principles that can be freely used for fast microarchitecure design. The hearth of the project is a single-issue in-order Rocket core, strictly coupled with a SIMD accelerator.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   NoCs
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Network-on-chips}
Network-on-chips (NoCs) are an emerging interconnection model in modern architectures, born as a novel solution to the bandwidth and latency bottleneck of tradition shared buses infrastructures. 

A network-on-chip is mainly composed of three elements, hereafter summarized. The first is the link which physically connects the nodes (or tiles) and actually implements the real communication. A physical link is composed of a set of shared wires, that connect two adjacent routers of the network. Links might be equipped with one or more logical or physical channels and each channel is composed of a separate set of wires. At the link level is defined the concept of \textbf{flit}, short for flow control unit \cite{peh2009chip}, which flows through physical links. Often, flits are the atomic units that form a stream. In some cases, they are further divided into smaller \textbf{phyts}, that better match the physical link width.

The second block is the router, which implements the communication protocol, and routes packets over the physical links. A router receives packets from a shared links and, according to the header in each packet, it sends the packet to right output link, splitting it into multiple flits. A NoC router is composed of a number of input ports (connected to shared NoC channels), a number of output ports (connected to possibly other shared channels), a switching matrix connecting all input ports to all output ports, and a local port to access the IP core connected to this router \cite{patterson}. At the router level is defined the concept of \textbf{packet}, which consists of a head flit that contains the destination address, body flits and a tail flit that indicates the end of a packet. In addition to this physical connection infrastructure, the router also implements logic blocks dedicated to the flow control policies, that define the overall strategy for flowing data though the NoC.

The flow control policy characterizes the packet movement along the NoC and as such it involves both global (NoC-level) and local (router-level) issues. Routers typically use a distributed control, where each router makes decisions locally. \textbf{Virtual channels} (VCs) are an important concept related to the flow control. Those multiplex a single physical channel over several logically separate channels (so called virtuals) with individual and independent buffer FIFOs. The main goal of a VC implementation is to improve performance and to avoid deadlocks, optimizing the physical channel usage \cite{bjerregaard2006survey}. To reduce the buffering requirements, routers implement a flit-based flow control mechanisms exist. Widely-adopted is the \textbf{wormhole} flow control, which allocates buffers on a flit granularity, a flit can be forwarded as soon as it arrives, there is no need to wait the whole packet. Hence, a packet composed of many flits can potentially span several routers, which might result in many idle physical links. 

The third and last block is the network interface (NI) or adapter. This element interconnects the NoC IP, such as cores, or coherence actors, to the NoC router. At the NI level is defined the \textbf{application messages}, which is decomposed in multiple packets by the NI and injected to the NoC router.

\subsection{Real on-chip Networks}
In the last years, HPC-oriented projects based on network-on-chip are increasing. The Tilera TILE64 processor \cite{bell2008tile64} is a multicore targeting the high-performance demands of a wide range of applications. This architecture supports a shared memory space across 64 tiles. It deeply leverages on four mesh networks to track coherence. 

The Kalray MPPAR-256 is a many-core processor that integrates 256 user cores and 32 system cores, which targets embedded applications whose, as media processing, traditionally numerical kernels, and time-triggered control systems. The cores are distributed across 16 compute clusters of 16+1 cores \cite{de2013distributed}, organized in a 2D torus network. Each cluster owns its private address space, while communication and synchronization among different cores is ensured by data and control Networks-on-Chip.

The Intel TeraFLOPS \cite{eldred1998design} is a research prototype that is targeted at exploring future processor designs with high core counts, implementing up to 80 tiles in a single network-on-chip. Different tiles communicate using using the Message Passing Interface (MPI).

The IBM Cell \cite{kahle2005introduction} architecture is meant to target a power-efficient gaming systems, but that are general enough for other domains as well. The Cell is a product that is in most game consoles, such as Sony PS3. It consists of one IBM 64-bit Power Architecture core and 8 Synergistic Processing Elements (SPEs), each of them are SIMD-based. These nine nodes are interconnected with an on-chip network, called Element Interconnect Bus (EIB), which overlays bus access semantics on four ring networks.

The STNoC \cite{grammatikakis2008design} is a prototype architecture and methodology from ST Microelectronics, which aims to replace the STBus in MPSoCs. It is targeted towards the unique demands of MPSoCs on the on-chip network fabric: automated synthesis of network design, compatibility with heterogeneous, diverse IP blocks from other vendors and prior knowledge of traffic demands. It has been applied to the Morpheus chip targeting 90nm, with eight nodes on the on-chip network connecting an ARM9 core, an array of ALUs, a reconfigurable array, embedded FPGA, and memory controllers.

The Aethereal \cite{goossens2005aethereal} is a NoC proposed by Philips, implemented in a synchronous indirect topology with wormhole switching, and contention-free source routing algorithm based on time division.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%%   Coherence
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Cache Coherence} \label{sec:cohe}
%%
%% 1 - Baseline protocol 
%% 2 - Modern architecture and support
%% 3 - Moving to sparse Directory
%% 4 - Scaling up: coherency regions and other optimizations
%%
\subsection{Incoherence Issues}
Traditional architectures are based on a baseline system model which includes a single multicore processor chip and off-chip main memory. When shifting toward multi- and many-cores processor-chip consists of multiple cores, often with both multi-threading and SIMD supports, each of which has its own private data cache, while a last-level cache (LLC) is shared by all cores, and it is considered a ``memory-side cache''. The LLC, also referred as memory controller, is logically in front of the memory and serves to reduce the average latency of memory accesses and increase the memory effective bandwidth, without introducing a further level of coherence issues. The cores and the LLC communicate with each other over an interconnection network.

In such scenarios, engineers and hardware designers have to deal with the possibility of \textbf{incoherence}, which arises because in modern architectures there exist multiple active entities, such as processors or DMA engines, with shared resources, that can read and/or write concurrently to caches and memory. This happens even in the simpliest multi-core system, which has only two concurrent cores with private data caches that share the same main memory. 

A simple example with two cores (namely \textbf{c$_0$} and \textbf{c$_1$}) would better explain the incoherence problem. Let assume that a memory location A stores the value $x$, and then both cores load this value from memory into their respective data caches. First, \textbf{c$_0$} turns whit some operations the value at memory location A to $y$, and stores the result into its cache. The other core has no idea that the value of A has been modified by \textbf{c$_0$}, thus this makes the other core copy of A in its cache inconsistent and, thus, incoherent. To prevent incoherence, the system must implement a cache \textbf{coherence protocol} to regulate the actions of concurrent cores such that \textbf{c$_1$} cannot observe the old value of $x$ at the same time that \textbf{c$_0$} observes the new value $y$, furthermore it is completely impractical to provide direct access from one processor to the cache of another core. The practical alternative is to transfer the updated value of A over to the other processor in case it is needed \cite{drepper2007every}. 

Modern coherence protocol are based on the \texttt{single}\texttt{-writer} \texttt{multiple}\texttt{-reader} (\texttt{SWMR}) invariant \cite{sorin2011primer}. This poses the basis of modern coherence: for any memory location, at any given moment in time, there is either a single core that may write it (and that may also read it) or some number of cores that may read it (but none of them can write it). Thus, there is never a time when a given memory location may be written by one core and simultaneously either read or written by any other cores \cite{sorin2011primer}. %Another way to view this definition is to consider, for each memory location, that the memory location lifetime is divided up into epochs. In each epoch, either a single core has read–write access or some number of cores (possibly none) have read-only access. 

Given the coherence basic definition, coherence protocols are highly influenced by the implemented cache features, such as the given write policy. When a cache line is modified, the result for the system after this point ought be the same as if there were no cache at all and the main memory location itself had been modified. This can be implemented in two ways or write policies, namely \texttt{write-through}, or \texttt{write-back} cache implementations.

The \texttt{write-through} cache is the simplest way to implement cache coherency and ease the protocol itself. Whenever a cache line is written to, the processor immediately also writes the corresponding line into main memory. This ensures that the main memory and cache are in sync at any time. The cache content could simply be discarded whenever a cache line is replaced, heavily simplifying the coherence protocol during evictions and replacements. However, this write policy is simple but not really fast. A program which, for instance, modifies a local variable over and over again would create a lot of traffic over the bus through the main memory even though the data is likely not used anywhere else. 

The \texttt{write-back} policy is more sophisticated. The processor does not immediately write the modified cache line back into main memory. Instead, the cache line is marked as dirty. Whenever a dirty cache line is dropped from the cache, due a replacement or an explicit eviction, the dirty bit states that the processor ought to write the data back at that time instead of just discarding the content. Write-back caches significantly perform better that write-through, resulting in a much less traffic flooding over the bus through the main memory. However, the coherence protocol have to deal with this policy becoming more complicated.

\subsection{Coherence States}

A commonly used approach to cache coherence encodes a permission to each block stored in the cache controller. Before a processor completes a load or a store, it must hit in the cache and the requested block must hold the appropriate permissions. If a processor stores to a block that is cached by other processors, it must acquire store permission by revoking read permission from other caches, in the respect of the \texttt{SWMR} invariant.

\begin{table}[ht]
	\centering
	\resizebox{0.8\columnwidth}{!}{
	\begin{tabular}{r|c|l}
		State & Permissions & Meaning    \\
		\hline
		\multirow{2}{*}{Modified (\textbf{M}) }	  &  \multirow{2}{*}{read and write} &  The cache line is dirty. \\ & &It is the only copy.  \\  
		\multirow{2}{*}{Shared (\textbf{S})} &  \multirow{2}{*}{read only} & The cache line is not modified \\ & & and might be shared.   \\
		\multirow{2}{*}{Invalid (\textbf{I})} &  \multirow{2}{*}{none} & The cache line is invalid, \\ & & and not cached. \\
		\multirow{2}{*}{Exclusive (\textbf{E})}  & \multirow{2}{*}{read and write} &  The cache line is not dirty \\ & & and has no other sharers. \\
		\multirow{3}{*}{Owned (\textbf{O})}     & \multirow{3}{*}{read only} & The processor is the owner of a block,\\ & & and it is responsible for responding to\\ & & coherence requests for that block.  \\
 	\end{tabular}
	}
	 \caption{Coherence stable states.}
	 \label{tab:stablestates}
\end{table}

Permissions in a cache are reflected by a coherence state stored in the cache info for a block. Typical cache coherence states, used in most existing protocols \cite{sweazey1986class}, are summarized in Table \ref{tab:stablestates}. The baseline protocol uses just three of the listed states, namely \texttt{MSI}, which represents the minimum set that allows multiple processors to simultaneously read a block line, or to denote that a single processor holds write permission. Other protocols, such as \texttt{MOSI} or \texttt{MOESI}, use the \texttt{O} and \texttt{E} states, but they are not as basic. In particular, \texttt{E} and \texttt{O} are used to implement protocol-level optimizations.

Initially all cache lines are empty and marked as Invalid. If data is loaded into the cache for writing the cache changes to Modified, the state for that block shifts from \texttt{I} to \texttt{M} (\texttt{I}$\rightarrow$\texttt{M}). If the data is loaded for reading the new state depends on whether another shares or/and the protocol has no \texttt{E} state, the new state is Shared (\texttt{I}$\rightarrow$\texttt{S}). Otherwise, if there is no sharers and the protocol supports the \texttt{E} state, the block line turns into the Exclusive state (\texttt{I}$\rightarrow$\texttt{E}). 

When a processor has a cache line in Modified state and a second actor requests to read from the this cache line, the first processor has to send the content of its cache to the second processor and then it downgrades the line state to Shared (\texttt{M}$\rightarrow$\texttt{S}), losing the write permission. The data sent to the second processor is also received and processed by the memory controller which might store the content in memory. 

On the other hand, if the second processor wants to write to the cache line the first processor sends the cache line content and marks the cache line locally as Invalid (\texttt{M}$\rightarrow$\texttt{I}), forcing the first processor to load back the cache line updated (if still needed) from the new owner. The \texttt{M}$\rightarrow$\texttt{I} transition is highly expensive in term of time, and for write-through caches we also have to add the time it takes to write the new cache line content to the LLC or the main memory, further increasing the cost.

If a cache line is in the Shared state and the local processor reads from it no state change is necessary and the read request can be fulfilled from the cache. If the cache line is loaded and in state Shared, and the processor locally writes it, the loaded cache line can be used as well but the state changes to Modified (\texttt{S}$\rightarrow$\texttt{M}). It also requires that all other possible copies of the cache line among the sharers are marked as Invalid (\texttt{S}$\rightarrow$\texttt{I}) in compliance with the \texttt{SWMR} invariant. Therefore the write operation has to be announced to the other sharers via an coherence message over the bus. 

On the other hand, if the cache line is in the Shared state and another core requests it for reading nothing has to happen to the other sharers. The requestor updates its state to Shared and load the data in the cache, while the main memory contains the current data and the local state is already Shared. 

The Exclusive state shares the same feature of the Shared state, although it has a substantial difference: a local write operation does not have to be announced on the bus. The local cache is known to be the only one holding this specific cache line. This can be a huge advantage so the processor will try to keep as many cache lines as possible in the Exclusive state instead of the Shared state. The latter is the fallback in case the information is not available at that moment. The Exclusive state can also be left out completely without causing functional problems. It is only the performance that will suffer since the \texttt{E}$\rightarrow$\texttt{M} transition is much faster than the \texttt{S}$\rightarrow$\texttt{M} one.

\subsection{Coherence Transactions}
Most protocols have a similar set of transactions, because the basic goals of the coherence controllers are similar, summarized in the Table \ref{tab:trans} hereafter:

\begin{table}[ht]
	\centering
	\resizebox{0.8\columnwidth}{!}{
	\begin{tabular}{r|l}
		Transaction & Effects \\
		\hline
		get Shared (\textbf{getS})	&  The processor requires the block for reading. \\
		get Modified (\textbf{getM})&  The processor requires the block for writing. \\
		\multirow{2}{*}{Upgrade (\textbf{Upg}) }	&  The processor upgrades the block line state  \\ & from a read only state to read and write.  \\ 
		put Shared (\textbf{putS})	&  The processor evicts the block from Shared. \\
		put Modified (\textbf{putM})	&  The processor evicts the block from Modified. \\
		put Exclusive (\textbf{putE})	&  The processor evicts the block from Exclusive. \\
		put Owned (\textbf{putO})	&  The processor evicts the block from Owned. \\
	\end{tabular}
	}
	\caption{Coherence transactions.}
	\label{tab:trans}
\end{table} 

Transaction messages flow over the bus after processors requests. E.g., if a processor’s read request misses in its cache, the block is in the Invalid state, the processor issues a \texttt{getS} coherence request over the bus to obtain data for read permission. According to the coherence protocol, the processor must obtain the most up-to-date data to that block and ensures that write permission is revoked from other processors. Consequently, any processor in one of states \texttt{M}, \texttt{O}, or \texttt{E} must supply the data and have to shift into a state with read-only permission (such as \texttt{O} or \texttt{S}). However if no processor has this block in read-write or read only state (namely \texttt{M}, \texttt{O}, \texttt{S}, or \texttt{E}), then the data should be fetched from the main memory.

On the other hand, if a processor misses in its cache for a write, or the block is not in state \texttt{M} or \texttt{E}, the processor issues a \texttt{getM} coherence request. The coherence protocol must obtain the most recently stored data to that block, like in the \texttt{getS} case, but also ensures all sharers drop the read permission for that block. If the requestor already holds the data in read-only permission, a possible optimization implements an \texttt{Upg} message that only invalidates other sharers caches and there is no need to retrieve the data.

\subsection{Snooping}

The \texttt{snooping protocols} is the most traditional and widely-used coherence protocol, its simplicity make it perfect for few number of cores. It is based on the idea that all coherence actors observe (or snoop) all flowing coherence requests over the shared system bus in the same order and consequently ``do the right thing'' according to the protocol to maintain coherence. Fundamental is that all requests to a given block arrive in order, a snooping system enables the distributed coherence controllers to correctly update the finite state machines that collectively represent a cache block state. Often, snooping protocols rely on a shared single bus which eases the request ordering, connecting all components to an electrical, or logical, set of wires. Such buses ought to provide atomicity such that only one message appears on the bus at any time and that all actors observe the same message \cite{goodman1983using}. 

A processor of a snooping protocol broadcasts requests to all coherence controllers, including its own. As stressed before, the ordered broadcast ensures that every coherence controller observes the same series of coherence requests in the same order, which guarantees that all coherence controllers correctly update the interested cache block state. With all coherence messages broadcast on a bus and with message arrivals ordered the same way for all nodes, coherence controllers at each node implement a state machine to maintain proper coherence permissions and to potentially respond to a request with data. E.g., when a processor requires a block for writing, it sends a \texttt{getM} request on the bus. As soon as the request appears on the bus, all other nodes snoop their caches. If the tag exists in a processor cache in state  \texttt{S}, the coherence state is changed to \texttt{I} in order to revoke read permission, forcing the processor to load back the data updated, and the data can be fetched from the LLC. If a processor cache contains a tag in state with exclusive access or/and write permission (such as \texttt{M}, \texttt{E}, or \texttt{O}), it is responsible to provide the most up-to-date date, inhibiting the LLC response, and then sending data on the bus before invalidating its cache tag and updating the state to \texttt{I}. The LLC response inhibition is an important function in a bus-based protocol, which states when the memory controller should not respond with data that is modified in a processor’s cache, often this functionality is provided by a shared line on the bus. 

Replacements in a bus-based snooping protocol are straightforward. Copies in read only states (such as \texttt{E} and \texttt{S} states) can be silently replaced, dropping the state and by taking no further actions. To write back dirty data to the LLC, the node must initiate a writeback transaction that contains the data and is accepted by the LLC. The atomic nature of the bus ensures that racing coherence requests are ordered with respect to the writeback operation.

%\subsection{Directory}

%\subsection{Coherence In Modern Architectures}

