\section{Parallelism in CPU and the ILP Wall} \label{sec:ilp}

A processor that executes every instruction sequentially, where instruction $i+1$ is fetched when instruction $i$ is complete, may use processor resources inefficiently, potentially leading to low performance. The performance can be improved by executing different sub-steps of sequential instructions simultaneously (technique named pipelining), or even executing multiple instructions entirely simultaneously as in superscalar architectures (VLIW). 

Pipelining is the first and most used solution. All current processors use pipelining to overlap the execution of instructions and improve performance. It is an implementation technique whereby multiple instructions are overlapped in execution. Such a solution takes advantage of parallelism that exists among the actions needed to execute an instruction.  This potential overlap among instructions is called instruction-level parallelism (ILP), since different instructions can be evaluated in parallel. A processor has different components, an instruction must pass through all these components to execute. When a stage has done instruction passes to the next and the previous component is unused. In pipelining all processor components are active every clock cycles. Each instruction must pass through all pipeline stages, and in each stage serves a different instruction. 

Pipelining can be convenient if its overhead is not very big, which arises from the combination of pipeline register delay and clock skew. The pipeline registers add setup time, which is the time that a register input must be stable before the clock signal that triggers a write occurs, plus propagation delay to the clock cycle. Clock skew, which is maximum delay between when the clock arrives at any two registers, also contributes to the lower limit on the clock cycle. Furthermore stages should have similar speed, otherwise this technique does not benefit. 

Historically, pipelining has been the key implementation technique used to make fast CPUs \cite{patterson}, although structural/data hazards and control dependencies force to stall the pipeline, significantly decreasing the final throughput. Modern architectures rely on more sophisticated hardware solutions to increase performance. Further improvement can be achieved by introducing multiprocessors, thread-level parallelism, and SIMD architectures.

\subsection{Very Long Instruction Word}

Superscalar processors use multiple, independent functional units, each clock cycle just one of them is really busy, the others are idle. To keep the functional units busy, there must be enough parallelism in a code sequence to fill the available operation slots. VLIW processors, on the other hand, issue a fixed number of instructions formatted either as one large instruction or as a fixed instruction packet with the parallelism among instructions explicitly indicated by the instruction. VLIW processors are inherently statically scheduled by the compiler. VLIW instructions are usually at least 64 bits wide, and on some architectures are much wider, in instance if a VLIW processor supports five parallel operations, the instruction would have a set of fields for each functional unit, e.g. 16–24 bits per unit, yielding an instruction length of between 80 and 120 bits. To combat this code size increase, clever encodings are sometimes used. For example, there may be only one large immediate field for use by any functional unit. Another technique is to compress the instructions in main memory
and expand them when they are read into the cache or are decoded. 

Superscalar CPUs use hardware to decide which operations can run in parallel at runtime, while in VLIW CPUs the compiler decides which operations can run in parallel in advance. Because the complexity of instruction scheduling is pushed off onto the compiler, complexity of the hardware can be substantially reduced.

This type of processor architecture is intended to allow higher performance without the inherent complexity of some other approaches.

%\subsection{SIMD paradigm and Vector processors}

\subsection{Multiprocessor and Thread-Level Parallelism}
\begin{figure}[t]
    \centering
    \includegraphics[scale=0.26]{Chapters/Background/Figures/multithreading.png}
    \caption{Superscalar, fine MT, coarse MT and SMT (credits \cite{patterson})}
    \label{fig:smt}
\end{figure}

Thread-level parallelism (TLP) is an higher-level parallelism and it is logically structured as separate threads of execution. A thread is a separate process with its own instructions and data. A thread may represent a process that is part of a parallel program consisting of multiple processes, or it may represent an independent program on its own. It allows multiple threads to share the functional units of a single
processor in an overlapping fashion. To permit this sharing, the processor must duplicate the independent state of each thread. 

Thread-level parallelism is an important alternative to instruction-level parallelism primarily because it could be more cost-effective to exploit than
instruction-level parallelism. There are many important applications where
thread-level parallelism occurs naturally, as matrix multiplication.

There are three main approaches to multithreading, shown in Figure \ref{fig:smt}. The first one is fine-grained multithreading switches between threads on each instruction, causing the execution of multiple threads to be interleaved. This interleaving is often done in a round-robin fashion, skipping any threads that are stalled at that time. To make fine-grained multithreading practical, the CPU must be able to switch threads on every clock cycle. One key advantage of fine-grained multithreading is that it can hide the throughput losses that arise from both short and long stalls, since instructions
from other threads can be executed when one thread stalls. A disadvantage of fine-grained multithreading is that it slows down the execution of the
individual threads, since a thread that is ready to execute without stalls will be delayed by instructions from other threads.

On the other hand, coarse-grained multithreading was invented as an alternative to fine-grained multithreading. Coarse-grained multithreading switches threads only on costly stalls, such as level 2 cache misses. This change relieves the need to have threadswitching be essentially free and is much less likely to slow the processor down,
since instructions from other threads will only be issued when a thread encounters a costly stall.

At last, Simultaneous Multithreading (SMT) is a variation on multithreading that uses the resources of a multiple-issue, dynamically scheduled processor to exploit TLP at
the same time it exploits ILP. The key insight that motivates SMT is that modern multiple-issue processors often have more functional unit parallelism available than a single thread can effectively use. Furthermore, with register renaming and dynamic scheduling, multiple instructions from independent threads can be issued without regard to the dependences among them; the resolution of the dependences can be handled by the dynamic scheduling capability. In the SMT, TLP and ILP are exploited simultaneously, with multiple threads using the issue slots in a single clock cycle. Ideally, the issue slot usage is limited by imbalances in the resource needs and resource availability over multiple threads. \\

 
%As these challenges became more apparent in the 1990s, CPU architects began
%referring to the \emph{power wall}, the \emph{memory wall} and the \emph{ILP wall} as obstacles to the kind of rapid progress seen up until that time.