\section{Networking System}
In a many-core system, the interconnection network has the fundamental goal of allowing various devices to communicate efficiently over the NoC. In such systems, different actors spread all over the network, require message exchanging. The networking system ease the message flowing, providing a simplified interface which allows communication among different tiles.
%The most common use case is the exchange of coherence and synchronization messages. A tile can contain multiple devices (called from now on network users) requiring network access. The network infrastructure must thus provide inter-tile and intra-tile addressing capabilities. The interface offered to its users must be as generic as possible, and independent of the specific network topology and implementation details.

The nu+ communication system relies on a light 2D mesh Network-on-Chip based on hardware router flit-based whit a wormhole control flow mechanism, which implements a XY routing. The nu+ networking system is mainly composed of a \texttt{Router} and a \texttt{Network Interface} (NI). In the default configuration the network provides $4$ virtual channels, $3$ of them are required by the coherence protocol, while the last is shared between the synchronization and boot mechanisms.

\subsection{Router}
The networking system works under the assumption that no flit can be lost. In order to avoid such situations routers are equipped with buffers, that eventually stall neighbours in case of full output buffers. This mechanism along with a back-pressure control flow ensures that no packet is drop. %In this process, as routers wait for each other, a circular dependency can potentially be created. As routers cannot drop packets to free buffer slots and allow a deadlock to be solved, we must prevent it from happening.

As a packet is routed through the mesh, a flit can enter a router and leave it from any cardinal direction. It is obvious that routing flits along a straight line cannot form a circular dependency. For this reason only turns must be analyzed. The simplest solution is to ban some possible turns, in a way that disallows circular dependency.

The routing protocol adopted in nu+ is the XY Dimension-Order Routing, or DOR. It forces packet to be routed first along the X axis, and then along the Y axis. It is one of the simplest routing protocols, as it takes its decision independently of current network status, and requires little logic to be implemented, although offering deadlock avoidance and shortest path routing.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Arch/figures/router}
	\caption{Baseline hardware router.}
	\label{fig:router}
\end{figure}

The router provides $5$ ports, one per cardinal direction (namely NORTH, SOUTH, WEST, EAST) plus one which connects the router to the local Network Interface of the tile. The router architecture implements a flit-based flow control, a dimension-ordering routing look-ahead mechanism for a 2D-mesh topology, and supports on-off back-pressure signals.
A look-ahead routing mechanism allows the next hop calculation one router in advance compared with a plain version. When the packet flows in a router, this already contains information of its next hop. This approach allows to merge the virtual channel and switch allocation in one stage and the router can perform resources allocation and next hop calculation concurrently.
To further reduce the pipeline depth, the crossbar and link traversal stage are not buffered, reducing the stages at two and merging the last stage to the first one.
%The router is part of a mesh, so it must have an I/O port for each cardinal direction, plus the local injection/ejection port. Each port exchanges flits with other routers. Flits are routed using the XY DOR protocol, using look-ahead. This means every router will do the routing as if it were the next one along the path. This will allow us to reduce the pipeline length, improving requests latencies.

Four virtual channels are required, to classify different packet types. On/Off back-pressure signals are generated for each virtual channel. The following rules must be ensured:

\begin{enumerate}
	\item A flit cannot be routed on a different virtual channel.
	\item Different packets cannot be interleaved on the same virtual channel.
\end{enumerate}

The router is implemented in a pipelined fashion, Figure \ref{fig:router} shows the nu+ router overview. Although three stages are presented, the last one's output is not buffered. This effectively reduces the pipeline delay to two stages.

The first stage is the \texttt{Input Buffer} which instantiates two queues, the first one stores flits (FQ) while the other stores only the current head flit (HQ). Each virtual channel allocates an Input Buffer. 
In the flit header are stored information required for routing and resources allocation. The allocation unit grants access to a flit flow if the required output port of a specific virtual channel is available, and handles the contention of virtual channels and crossbar ports among different flows. When a flit flow wins resource allocation, it can cross the router through the crossbar.
The second stage is composed of three blocks: the allocator, the flit manager and the routing logic. The allocator manages the allocation of the third stage ports, generating a grant signal for each virtual channel allowed to proceed, considering also back-pressure signals coming from other routers. This grant is used by the flit manager to select the winning flits, which are fed to the next stage along with the routing informations generated by the routing block.
The third stage is the crossbar, which connects each of the $5$ input ports to each of the $5$ output ports.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.98\textwidth]{Chapters/Arch/figures/ni.eps}
	\caption{Overview of the Network Interface design.}
	\label{fig:ni}
\end{figure}

\subsection{Network Interface} \label{sec:NI}

Cache coherence protocols are usually based on the main assumption single-writer, multiple-reader invariant. Any nodes may have a copy of memory in its cache to read from. When a node wishes to write to that memory address, it ought to ensure that no other nodes are caching that address, in the total respect of the \texttt{SWMR} invariant. The resulting communication requirements for a shared memory multiprocessor consist of three kinds of message, namely \texttt{requests}, \texttt{responses} and \texttt{forwarded} messages. These do not rely on networking ordering, although sharing the same channel can incur in protocol deadlocks \cite{peh2009chip}. E.g. a request message from the network cannot be processed until the block receives the response from the directory. If responses utilize the same network resources as requests, those replies cannot make forward progress resulting in deadlock.

Virtual channels are an extensively adopted technique coupled with the directory-based coherence protocol, solving such protocol deadlocks in network design, allowing multiple virtual networks starting from a single physical channel.

The hardware router has no view of the original message, it is up to the NI (shown in Figure \ref{fig:ni}) to convert the application message in a stream of flits. It splits a packet into multiple flits and injects these into the network and vice-versa. Specifically, in the baseline implementation, the Network Interface provides access to the mesh at directory controller, cache controller and service units (such as boot manager, barrier core unit, synchronization manager). The Network Interface can be summarized as follows:

\begin{enumerate}
	\item Conversion from packet to flits: it buffers the input application message, and converts a packet into multiple flits. If the packet contains data a stream of $9$ flits is generated, otherwise a single Head-Tail flit is produced.
	The NI also provides multicast support, sending $k$ times a packet in unicast to multiple destinations.
	\item Packet reconstruction: the NI reads and buffers every incoming flit from the local router, then builds back the original packet. 
\end{enumerate}

In the nu+ networking system the number of virtual channels is represented by the constant parameter, currently set to $4$, as many as the type of network messages: requests, responses, and forwards message for achieving coherence transaction deadlock free; and a last called service generally used for host communication. As a given message type is associated with a specific virtual channel, the network interface exposes a dedicated I/O port to each of them. A component, which generates request messages, must interface to the request NI port in order to inject its application messages over the network. Every virtual channel has a dedicated input buffer, so flow control is also be implemented on a virtual channel basis. This means that every router sends to its neighbours informations regarding the status of its buffers, for each virtual channel, implementing a on/off back-pressure control at the virtual channels level. 