\section{Coherence sub-system}

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{Arch/figures/LDST_CC.eps}
	\caption{The nu+ coherence sub-system overall}
	\label{fig:ldst_cc}
\end{figure}

Nu+ cores can be arranged as a many-core architecture based upon a shared-memory subsystem. With the shared-memory model, communication occurs implicitly through the loading and storing of data and the accessing of instructions. Logically, all processors access the same shared memory, allowing each to see the most up-to-date data. Practically speaking, memory hierarchies use caches to improve the performance of shared memory systems. These cache hierarchies reduce the latency to access data but complicate the logical, unified view of memory held in the shared memory paradigm. As a result, cache coherence protocols are designed to maintain a coherent view of memory for all processors in the presence of multiple cached copies of data. Therefore, it is the cache coherence protocol that governs what communication is necessary in a shared memory multiprocessor. 
Two key characteristics of a shared memory multiprocessor shape its demands on the interconnect; the cache coherence protocol that makes sure nodes receive the correct up-to-date copy of a cache line, and the cache hierarchy.

\subsection{Cache Hierarchy}

Caches are employed to reduce the memory latency of requests. They also serve as filters for the traffic that needs to be placed on the interconnect. Each of the tiles in Nu+ many-core architecture contain a bank of shared L2 cache. With a shared L2 cache, a L1 miss will be sent to a L2 bank determined by the miss address (not necessarily the local L2 bank), where it could hit in the L2 bank or miss and be sent off-chip to access main memory.

Shared caches represent a more effective use of storage as there is no replication of cache lines. However, L1 cache miss incur additional latency to request data from a different tile. Shared caches place more pressure on the interconnection network as L1 misses also go onto the network, but through more effective use of storage may reduce pressure on the off-chip bandwidth to memory. With shared caches, more requests will travel to remote nodes for data. Using this configuration the on-chip network must attach to both the L1s and the L2.

Memory controllers are placed as individual nodes on the interconnection network; with this design, memory controllers do not have to share injection/ejection bandwidth to/from the network with cache traffic. In this way traffic is more isolated; the memory controller has access to the full amount of injection bandwidth.

\subsection{Architectural Details}

The coherence sub-system is composed of three components further described in this chapter:

\begin{enumerate}
	\item load/store unit: contains L1 data cache;
	\item cache controller: handles L1 coherence data cache and manages coherence transactions;
	\item directory controller: handles L2 cache and manages coherence transactions.
\end{enumerate}

Load/store unit and cache controller are part of nu+ core while directory control is allocated on the tile level.

\subsection{L1 Cache Assumptions}

L1 cache design has been driven by these assumptions:

\begin{enumerate}
	\item if a thread raises a cache miss, the thread is suspended until this request is fulfilled by L1 coherence controller;
	\item merging of requests from the same core is forbidden;
	\item it is possible to have only N*N networks (with N be a power of two); this implies empty tiles have to be introduced and that these tiles has a portion of the L2 cache and directory.
\end{enumerate}


\subsection{Load/Store unit}

Load/Store unit handles all data memory operations using an L1 N-way set-associative cache allocated inside. All cache parameters are configurable, such as number of sets and number of ways. It implements a non-blocking miss mechanism. When a memory miss occurs, the core can still execute instruction on different data, while the Load/Store unit is retrieving the data from the memory hierarchy. %The Load Store Unit does not store specific coherence protocol information (stable states), but it stores privileges for all cached addresses. Each cache line has two privileges: can read and can write. Those privileges determine cache misses/hits and are updated by the Cache Controller. 
It interfaces the Operand fetch and Writeback stages on the core side, dually on the bus side it communicates with the cache controller which updates information (namely tags and privileges) and data, as shown in Figure \ref{fig:ldst}. Moreover, such a module sends a signal to the instruction buffer unit which has the purpose of stopping a thread in case of miss. Load/Store unit does not store any information about the coherency protocol used, although it keeps track of information regarding privileges on all cache addresses. Each cache line stored, in fact, has two privileges: can read and can write that are used to determine cache miss/hit and are updated by the Cache Controller. Finally, it should be noted that this unit does not manage addresses that refer to the IO address space: whenever an issued request belonging to the non-coherent memory space is directly forwarded to the Cache Controller which will handle bypassing the coherency protocol, sending the request to the memory controller and report the data back to the third stage.

\begin{figure}[!ht]
	\centering
	\includegraphics[width=1\textwidth]{Arch/figures/LDST.eps}
	\caption{Load/store unit detailed view.}
	\label{fig:ldst}
\end{figure}

This unit is divided in three stages:

\subsubsection{Stage 1}
The first stage queues every request from the Operand Fetch. It instantiates a FIFO per thread in which are stored the issued thread instruction. When the second stage accepts an instruction from the i-th thread, it asserts the i-th bit of the \texttt{ldst2\_dequeue\_instruction} mask. The i-th bit of this mask is connected to the dequeue port of the i-th FIFO, and the instruction is popped from its queue. Before the requests are queued, a phase of decoding and verification of the type of operation is performed (to understand if it is a word, half word or byte), afterwards the control logic checks the alignment of the incoming request. Alignment is done based on operations (both scalar and vector) on byte, half-word and word. In case of vector alignment, the data is compressed so that it can be written consecutively. For example, a vec16i8 - that has 1 significant byte each 4 bytes - is compressed to have 16 consecutive bytes. Then, the instruction in the FIFO related to that thread is enqueued into its corresponding queue, but only if the instruction is valid or a flush request, and if there is no rollback occurring. This stage checks address alignment: if an instruction requires an improper address, this stage asserts \texttt{is\_misaligned} signals [MERGIARE], discards the incoming request and a trap is raised. The first stage provides also a recycle buffer besides all the other FIFOs. If a cache miss occurs in the 3rd stage, the data is forwarded in this buffer. The output of this this buffer competes with the normal issued load/store instruction to be re-executed. The recycled instructions have the higher priority respect to the other pending operations.

Finally, a pending instruction for each thread is forwarded in parallel to the next stage. Whenever the next stage can execute an instruction provided by the i-th thread, it asserts the \texttt{ldst2\_dequeue\_instruction} signal that notifies the stage 1 whether the instruction has been consumed or not. In this way, it is possible to stall the instruction at the head of each FIFO if the next stage is busy. Moreover, stage 1 is equipped with a recycle buffer: if a cache miss occurs in the Stage 3, the instruction and its operands are saved in this buffer. The output of this buffer is in competition with the normal load/store instructions, but the recycled instructions have a higher priority than the other operations.

\subsubsection{Stage 2}
The second stage arbiters the FIFOs in the first stage, stores L1 cache tag and privilege information. This stage receives as many parallel requests from the core as the number of threads. A pending request is selected from the set of pending requests from the previous stage. A thread is chosen by a round-robin arbiter; first the selected thread index is obtained, if it has both a valid recycled and FIFO requests, then the former will be served first because it has higher priority.
When a memory request is scheduled, this stage forward to the next stage data, tag and privileges relative to the requesting address. These information are stored in SRAMs equipped with 2 read ports and 1 write port. A second reading port is provided because the Cache Controller snoops the cache information. The implemented SRAMs provide a read-first mechanism, in the worst scenario the Cache Controller updates tag or privileges of the same address that is requested by an already scheduled operation. With a read-first policy, the operation will retrieve the right information.
The second stage does not handle the cache information itself, this is up to the Cache Controller which has a preferential port and through this it sends data and commands to the second stage. 

The cache controller can send different commands to the Load/Store unit. In case of \textbf{CC\_INSTRUCTION}, the stage 2 receives an instruction that could not be served previously. The cache controller, in this case, provides the new privileges, tags and data. So, this stage bypasses the new data (privileges, tags and data) to the next one in order to ``complete'' the operation (whether it is a load or a store). In detail, when the output signal \texttt{ldst2\_valid} is asserted, it propagates the \texttt{ldst2\_instruction}, the new address is passed on \texttt{ldst2\_address}, the data is passed over \texttt{ldst2\_store\_value} signal, with related masks, tags and privileges.

In case of \textbf{CC\_UPDATE\_INFO} from the cache controller, stage 2 receives a command in order to update the information (tags and privileges) of a given set. In this case nothing is propagated and no validation signal is asserted to the next stage, since it is not necessary to update the data cache. The privileges and the cache tag are updated with the values provided by the cache controller. It also indicates which way must be updated being responsible for the pseudo-LRU.

In case of \textbf{CC\_UPDATE\_INFO\_DATA} from the cache controller, stage 2 receives an update command, but in this case must also be updated data cache. Stage 2 propagates the address information (which are used to identify the index relative to the set), way and value provided by the cache controller. As in the previous case, the privileges and the cache tag are updated with the values provided by the cache controller. In this case the notification is made by asserting \texttt{ldst2\_update\_valid} and stage 3 should not propagate anything to the Writeback stage. In detail, only \texttt{ldst2\_update\_valid} is asserted, the way is \texttt{ldst2\_update\_way}, the new address is passed using \texttt{ldst2\_address}, the data passed using \texttt{ldst2\_store\_value}.

In case of \textbf{CC\_EVICT} from the CC, stage 2 must notify next one that the data must be replaced. In this case the notification is made by asserting \texttt{ldst2\_evict\_valid} and providing the complete address of the data to be replaced (which will have the same index as the new one), the way to use and the data. In detail, \texttt{ldst2\_evict\_valid} and \texttt{ldst2\_update\_valid} are asserted, the data is passed on \texttt{ldst2\_store\_value}, the old address is composed of stage 3 using the past tag and the index of the new one, the way is \texttt{ldst2\_update\_way}.

The last responsibility of this stage is to wake a thread up when a cache miss occurs, thread that caused a cache miss is asleep (via the \texttt{ldst3\_thread\_sleep} signal);

\subsubsection{Stage 3}
The third stage detects if a cache miss or hit occur, and allocates the data cache, which is a SRAM with 2 read ports and 1 write port. When an instruction is issued, this stage checks privileges and tag and asserts ldst3\_miss output bit consequently. If a miss occurs the instruction is forwarded to the recycle buffer in the first stage and this module dispatches a missing request to the Cache Controller. The Cache Controller manages the data cache as in the previous stage, and has a privileged snoop port on it.

This stage receives input instructions, data update requests and replacement requests. Before managing logic to generate the output it is necessary to understand the type of request received (instruction, data update, replacement). Moreover, in case of instruction the control logic have to check if there is a hit or a miss and if there is a \texttt{load\_miss} or \texttt{store\_miss} (that occur if we do not have the necessary privileges). 

A cache hit is asserted if we have read or write privileges on such address, and if the tag of the requested address is equal to an element present in the tag array (\texttt{dcache\_way}) read from the tag cache using the address set (passed from the Stage 2). Then it must be verified if we have permissions to perform operations on the block for which there was a hit. E.g., if there is a hit for that address and the request is a store, we must have write permissions for that block otherwise there will be a store miss. Therefore, in case of store hit it saves data in the data cache (and does not send anything outwards), in case of load hit the \texttt{ldst3\_valid} signal is asserted, and data will be sent to the WriteBack module. In case of store/load miss the \texttt{ldst3\_miss} is asserted towards the cache controller. Furthermore, in the case of miss the thread must be put to sleep. In case of update operations, the data cache line identified by the signals pair \texttt{ldst2\_address.index} and \texttt{ldst2\_update\_way} is updated with the new data \texttt{ldst2\_store\_value} coming from previous stage.

Finally, for replacement operations, similarly to the update case, the cache line addressed by \texttt{ldst2\_address.index} and \texttt{ldst2\_update\_way} is updated with the new value \texttt{ldst2\_store\_value}, and its old content sent to the cache controller through the output ldst3\_cache\_line asserting the \texttt{ldst3\_evict} signal. In detail, are sent to the controller cache: the old address on \texttt{ldst3\_address}; the old cache line contents on \texttt{ldst3\_cache\_line} and \texttt{ldst3\_evict} is asserted.

Note that, there is a second read port for the data cache that is used by the cache for snoop requests (that is, the read requests that are used by the controller cache to read the data, is a sort of fast lane). This port is \textbf{WRITE FIRST} so the cache controller receives the latest version of the data even when we are doing a store operation on that cache line.