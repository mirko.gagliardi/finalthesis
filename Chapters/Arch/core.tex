\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/Arch/figures/nuplus_core_detail.eps}
	\caption{Overview of the core microarchitecture}
	\label{fig:core}
\end{figure}

\section{Core microarchitecture} \label{sec:micro}
The heart of the nu+ many-core platform is a RISC in-order core, oriented to highly data-parallel kernels with a lightweight control infrastructure, shown in Figure \ref{fig:core}.
Most of its resources are dedicated to computation-intensive operations on massive datasets, blending together a hardware multi-threading support with a SIMD paradigm.
The baseline core implementation presented in this dissertation is by default equipped with $8$ hardware threads, with a SIMD capability able to compute $16$ concurrent operations each cycle, and $64$ general purpose registers. Both data and instruction caches are $n$-way set-associative, with $4$ ways, $128$ sets each and a data width of $512$ bits by default.

All threads share the same compute units. Execution pipelines are organized in hardware vector lanes (like vector processors, each operator is replicated L times). Each thread can perform a SIMD operation on independent data, while data are organized in a vector register file. 

The core supports a high-throughput non-coherent scratchpad memory (SPM) corresponding to the shared memory in the NVIDIA terminology. The SPM is divided in a parameterized number of banks based on a user-configurable mapping function. The memory controller resolves bank collisions at run-time ensuring a correct execution of SPM accesses from concurrent threads. Coherence mechanisms incur a high latency and are not strictly necessary for many applications.

The remainder of this section focuses on the internal details of the baseline GPU-like core and on the networking subsystem. 

\paragraph{Thread Selection and Instruction Fetch}
%The Instruction Fetch stage is composed by two stages, its primary purpose is to schedule thread PCs and fetch the next instruction from the cache. Available threads are scheduled in a round-robin fashion by a dedicated arbiter. The Thread Controller has a privileged connection to the PCs pool inside the IF, in order to initialize each thread PC through a specific interface during the boot phase. When a rollback occurs from the Rollback Handler stage, the Instruction Fetch module overwrites the PC of the thread that issued the rollback. The instruction cache is N-way set-associative fully parameterizable. Number of sets, set width and number of ways can be configured in nuplus\_define.sv configuration file. The L1 instruction cache inside the Instruction Fetch is decomposed into different two SRAMs. The first stores information values such as TAGs and validity bits, while the second SRAM stores nu+ instructions. When a thread is selected, the Instruction Fetch stage reads its PC, and determines whether the requested cache line is already into the cache or not. The first stage first selects a thread and fetches all ways in parallel based on the requiring address, then passes these information to the second stage. The second stage determines the presence of the line in the cache based on the retrieved information and the ongoing request. In case of cache hit, this stage issues fetches the instruction from the cache and forwards it to the Decode stage. If a miss occurs, an instruction memory transaction is first issued to the Thread Controller which handles it, then the thread is blocked until the instruction line is not retrieved from the main memory. 

Each hardware thread has private internal resources such as PC, register file, and status/control registers along with a private memory stack, although all threads share the same compute units and L1 cache.
Specific thread information, such as current PC value, thread status, is handled by the \texttt{Thread Selection} unit in the first stage, which implements an interleaved multi-threading scheduling in a fine-grain way with a low architectural impact.
The Thread Selection unit issues an active thread to the next stage based on its information. At this stage, an internal round robin arbiter forwards to the \texttt{Instruction Fetch} the selected thread ID and its current PC value
in a fair mode after every cycle.
The \texttt{Thread Selection} updates the issued thread PC value according to the feedback signal from the \texttt{Instruction Fetch}, and in case of instruction cache miss the issued thread is stalled becoming no more eligible for scheduling.
Its PC value is not increased and a memory request for the requested instruction line is issued to the main memory subsystem, which can require up to $m$ cycles depending on the main memory latency.
When the data is gathered back from the main memory, the \texttt{Thread Selection} is notified and the previous stalled thread is reactivated.
In case of instruction hit, the \texttt{Instruction Fetch} retrieves the requested instruction from the cache, which flows along with the scheduled thread ID to the \texttt{Decode} unit in the next cycle.  

\paragraph{Decode stage}
This stage decodes fetched instruction from the \texttt{Instruction Fetch} module and produces the control signals for the datapath. This stage outputs a structure which helps modules to manage the issued instruction which is propagated in each pipeline stage. Such structure stores information such as PC, Thread ID, source and destinations registers. %Instruction type are presented in the ISA section. 
The heart of the module is a combinatorial case switch construct which fills all the field in that structure. If the scheduled instruction is valid, this stage asserts the valid instruction bit and forwards the decoded instruction to the Instruction scheduling stage.

\paragraph{Instruction Buffering and Dynamic Thread Scheduling}
Decoded instructions are stored in FIFOs allocated in the \texttt{Instruction Buffering} stage. For each pending instruction, the \texttt{Thread Scheduler} checks data hazard, stating which thread can be forwarded to the next stage. 

The control system relies on a lightweight scoreboarding mechanism for both data and structural hazard detection. The \texttt{Thread Scheduler} is the heart of such a logic. It updates the scoreboarding system stalling threads whenever hazards or data cache misses occur. The \texttt{Thread Scheduler} module is also in charge to check \texttt{Floating Point} unit structural hazards, as explained below. The FP pipeline has one output demultiplexer to the Writeback unit and is composed by operators with different latencies. If two concurrent operations terminate at the same time, they collide in the output propagation. 

Furthermore, this stage checks potential data hazards for the incoming thread and updates the scoreboard consistently with the issued instruction. At the same time, it checks if the current thread instruction raises a structural hazard on the \texttt{Writeback} stage. In the execution datapath different operators have different latencies (such as dividers and multipliers), therefore they can collide in the writeback operation.
At each clock cycle, the \texttt{Thread Scheduler} issues a schedulable thread to the execution datapath and, whenever hazards occur, it notifies back involved threads IDs to the \texttt{Thread Selector} which stalls them until those conditions are no more true.

If a rollback occurs, the FIFO of the involved thread are flushed, and the \texttt{Thread Scheduler} restores its scoreboard to the previous state.

\paragraph{Operand fetch stage}
%Operand Fetch fetches and builds operands for the Execution pipelines. As highlighted in the introduction, nu+ core supports both scalar and SIMD operations, for this reason Operand Fetch instantiates two register files: a scalar register file (called SRF) and a vector register file (called VRF).  Register size of both of them is parameterizable, in the default configuration the SRF instantiates 32-bit registers, and the VRF instantiates one scalar register for each hardware lane (`REGISTER\_SIZE x `HW\_LANE, default 32 bit x 16 hw lane). SRF and VRF share the same register number defined by `REGISTER\_NUMBER  parameter into nuplus\_define.sv configuration file (set to 64 by default).
Execution datapaths and register files are designed to exploit data-level parallelism in line with the SIMD paradigm. The \texttt{Register File Manager} fetches data from registers and composes operands. Each thread has both private scalar and vectorial register files. The latter can store up to $16$ scalar data, with a total width of $512$ bits in order to satisfy the execution pipeline data throughput.
Both register files are organized in compact SRAMs with two read and a write ports each, allowing two read and a write operations during each cycle.
The total number of registers allocated is proportional to the number of threads supported. In the default implementation each register file has $256$ registers, i.e. $64$ for each thread.
The control logic in this stage retrieves the right register subset based on the requesting thread ID, which feeds the high part of both read address ports. In the next cycle, operands are composed based on the instruction decoded information and issued to the execution pipelines along with thread and decoded information required by the computation units.
%Both register files receive in input the source registers addresses from the instruction scheduled. In the next cycle, the fetched registers are propagated to combinatorial blocks which prepare the two operands based on the instruction requirements. These blocks check if operands are scalar registers, vectors,targeting the PC or immediates (immediate values are decoded into the instruction), and fill the opf\_fecthed\_op0 and opf\_fecthed\_op1 output signals consequently. 

\paragraph{Integer Arithmetic and Floating Point unit}
%The Integer Arithmetic Logic unit is the principal execution module. It strips the decoded instruction and checks if the current operation is a jump, an integer calculation, a control register access, a vector shuffle, a move or a compare.  This module instantiates as many scalar integer ALU as hardware lanes defined by the `HW\_LANE parameter into the nuplus\_define.sv configuration file. The vectorial operation are executed in parallel using the concept of lane: each lane performs a scalar operation and the final vectorial result is composed by chaining all the scalar results from all lanes. If an operation is scalar, only the first lane is propagated. Only integer and compare operations can be both scalar and vectorial. When a control register access is dispatched, this module returns performances counter, such as data misses count, or static information, such as core or thread IDs.

The architecture implements an instruction set containing instructions that operate on arrays of data. Computational units are organized in hardware vector lanes, with each scalar operator being instantiated $16$ times.
The \texttt{Integer Execution} unit and the \texttt{Floating Point} unit share a similar organization, they both receive operands composed by the \texttt{Operand Fetch} organized in vectors, then they internally decompose each vector and feeds all the ALUs and FPUs with scalar operands.

In the \texttt{Integer Execution} unit, all the allocated ALUs perform the same operation in one clock cycle. On the other hand, floating point operators have different latencies, up to $32$ clock cycles due the divider in this implementation. Such a data parallelism allows each thread to perform SIMD operations on $16$ independent data simultaneously. The Integer Execution unit also contains the \texttt{Branch Control} module which handles jumps and rollbacks, flushing the control information for the involved thread in the pipeline when they occur and notifying the \texttt{Instruction Fetch} to update the thread PC.
%\paragraph{Floating Point unit}
%As already highlighted, the nu+ architecture can be easily configured with custom settings. These also include the Floating Point unit (FPU). In this section, we described a possible instantiation of the FPU in the nu+ system, based on a hardware blocks generated through the FloPoCo library. However, notice that depending on the actual application needs and available resources, different choices are possible with little changes to the surrounding core architecture. 

On the other hand, the implemented \texttt{Floating Point} unit supports scalar and vectorial operations IEEE-754 standard compliant. This stage is composed by different floating point operators with different latencies. Supported operators are baseline floating point units, such as adder, multiplier, divider, and comparators. There is no subtractor since, when a subtraction is dispatched, the unit reverts the sign of the second operand in the adder, so as to save hardware resources.
All floating point operators have a latency larger than $1$. The results are ready after a latency depending on the selected operator.
%\begin{table}[!h]
%	\centering
%	\resizebox{0.8\columnwidth}{!}{
%	\begin{tabular}{c|c|c|c|c}
%		        & Adder & Multiplier & Divider & Comparators \\
%		        \hline
%		Latency &  10   &     3      &   17    &      1
%	\end{tabular}
%	}
%\end{table}
%As in the Integer unit, this module instantiates as many scalar operators as hardware lanes, and the vectorial result is composed by merging all the results from all lanes.
All operator outputs are connected to the \texttt{Writeback} through the same demultiplexer, only one result for clock cycle can be dispatched to the next unit. This demultiplexer is handled by a pending queue, which selects the correct floating point output to forward. Such a queue is a vector with a length equal to the greatest latency among all operators (in this configuration it is set to 17 due the divider latency). When a new instruction is issued, the pending queue tracks this information, by storing the incoming instruction in a location of the vector equal to the latency of the operator (e.g. when an \texttt{FP\_ADD} is issued which has $10$ clock cycles of latency, pending\_queue[$9$] = \texttt{FP\_ADD}).  The queue shifts every clock cycle, when an instruction reaches the last position, the queue sets the demultiplexer selection port which propagates to the \texttt{Writeback} the right result. The decoded instruction and the fetched mask from \texttt{Operand Fetch} unit are delayed by the pending queue, in order to be forwarded to the \texttt{Writeback} along with the floating point result. If two different floating point instructions terminate at the same time, a structural hazard raises on the demultiplexer and one of the results is lost. The \texttt{Floating Point} unit does not support structural hazard control, this is demanded to the \texttt{Thread Scheduler} during the instruction issue phase.

The computational outputs from both the integer ALUs and floating-point units are gathered and reorganized in a vectorial form, then forwarded to the LSU module along with threads information. 

\paragraph{Branch unit}
The \texttt{Branch} unit manages conditional and unconditional jumps, restores scoreboards when a jump is taken, and forwards to the \texttt{Rollback Handler} the new PC value, the scoreboard to restore and the current thread ID. The scoreboard has to be restored in any case when a branch is taken: when a jump occurs two other instructions of the same thread could have been scheduled in the worst case. Those instructions are flushed if the branch is taken and the Rollback Handler undoes their executions.
Base address or branch condition are stored in the first scalar register of the first operand vector, immediate is stored in the first scalar register of second vector. The core supports two jump instruction formats:

\begin{enumerate}
	\item JRA: Jump Relative Address is an unconditional jump instruction, it takes an immediate and the core will always jump to PC + immediate location. E.g. \texttt{jmp -12} $\rightarrow$ BC will jump to $PC-12$ (3 instructions backward).
	\item JBA: Jump Base Address can be a conditional or unconditional jump, it takes a register and an immediate as input. In case of conditional jump, the input register holds the branch condition, if the condition is satisfied BC will jump to $PC +$ immediate location. E.g. \texttt{branch\_eqz s4, -12} $\rightarrow$ BC will jump if register s4 is equal zero to $PC-12$ location.
\end{enumerate}

In case of unconditional jump, the input register is the effective address where to jump. 

\paragraph{Rollback Handler}
The \texttt{Rollback Handler} restores PCs and scoreboards of the thread that issued a rollback. In case of jump or trap, the Branch unit issues a rollback request to this stage, and passes the thread ID, the old scoreboard state and the PC to restore.
Furthermore, the Rollback Handler flushes all ongoing and queued requests of this thread live in the core. Each thread has a bit value which states when a rollback on that thread is issued. Those bits are combinatorially connected to all flush ports of the threads FIFOs and to each module. When one of this bit is high due a rollback, the respectively FIFO is flushed. Dually, a module which is dealing with the rolling back thread discards the thread result and propagates a bubble to the next stage. In this way, a rollback issued by a thread does not affect others.

\paragraph{Writeback stage} \label{sec:wb-core}
The \texttt{Writeback} stage forwards computing results from the execution pipelines into the core registers. Components in the execution pipeline, such as FP or LDST unit, may have unpredictable latencies during the instruction issue phase. Instructions issued in different cycles might want to write in the register files at the same clock cycle. The register files provide only 1 write port, hence only one instruction per clock cycle can perform a write on them. The \texttt{Writeback} module avoids structural hazards on register files accesses on-the-fly. This mechanism is based on a set of queues ($4$ by default), one for execution module. The result from the corresponding component is stored in each queue. An arbiter selects one result in a round-robin fashion and forwards it into its destination register. Each queue stores all the information needed for a writeback operation, such as destination register and write mask.
Each of these queue has an almost full threshold, set equal to the number of cycles that separate the \texttt{Writeback} to the \texttt{Thread Scheduler}  which will stop issuing instructions to the specific component till this condition is true. Such a mechanism signals to the \texttt{Thread Selector} when the \texttt{Writeback} cannot store any further pending requests, avoiding operation-loss and providing an on-the-fly structural hazard detection solution. %If the almost full condition is true, this information is forwarded to the Dynamic Scheduler, which will not issues instructions to the specific component till this condition is true. A round-robin arbiter selects one of the pending output from all the queues. 

%Furthermore, in case of masked operation, the Writeback stage has to:
%
%\begin{itemize}
%	\item create a byte-grained register mask, in order to avoid the writing in undesirable byte (e.g. a \texttt{load\_8} operation writes only in the first byte and not in all the 4-byte register);
%	\item compose the final result, moving the bytes in the right positions and executing 8-bit/16-bit and 32-bit load sign extensions.
%\end{itemize}

\paragraph{Thread Controller}

\texttt{Thread Controller} manages the eligible threads pool. This module blocks threads that cannot proceed due cache misses or hazards. Dually, \texttt{Thread Controller} restores blocked threads when the blocking conditions are no more true.

A memory miss blocks the corresponding thread through the \texttt{ib\_fifo\_full} signal until the data is retrieved from the main memory.
Furthermore, the Thread Controller interfaces the Instruction Cache with the main memory, the architecture supports only one level of caching for instructions, in other words when an instruction cache miss occurs the data is retrieved directly from the main memory through the network. An instruction cache miss is handled by a 3-state FSM: the first state waits for a cache miss request. When it occurs the second state stores all the needed informations and issues a memory request to the main memory through the Network Interface.  The third state waits the memory response, when the data is fetched from the main memory it restores the blocked thread and backs to the first stage waiting for another request.
If during the memory waiting time another cache miss occurs for the same address of another request pending, a specific queue merges those two: it queues the cache misses and also merges requests to the same instruction address from different threads.
The third task performed is to accept the jobs from host interface and redirect them to the thread controller.

\paragraph{Load Store unit}
The \texttt{Load Store} unit is organized in a $n$-way set-associative write-back L1 cache strictly coupled with a light cache controller which implements a simple valid/invalid coherence mechanism.
Such cache controller handles misses and memory transactions and it also provides both request serialization and merging mechanisms in order to correctly manage concurrent requests from different threads.
The cache line width matches the internal hardware lanes capability, thus a read memory request loads $16$ scalar data from main memory and stores them into a vectorial register at once, minimizing requests and exploiting the internal parallelism of the SIMD accelerator.
On the other hand, the LSU is organized in three stages. The first stage receives the effective address calculated by the previous stage and, in case of data misses, it notifies the Thread Scheduler unit stalling the thread until the data is retrieved back from the main memory. The other two stages manage respectively coherence information and data.

Since, the \texttt{Load Store} unit is a fundamental component in this dissertation, it is detailed in the next chapter along with the rest of the coherence sub-system deployed.

\paragraph{Special Registers}
Control and special purpose registers are visible to the host controller. The architecture provides a dedicated interface which allows the manager to retrieve such registers in real-time during the execution without interfering with the kernel flow.
The baseline implementation is equipped with general performance counters, such as cache data misses occurred. 

\paragraph{Configurable Scratchpad Memory (SPM)}

The GPU-like core is equipped with a fast non-coherent user-managed on-chip memories, called scratchpad memories (SPM). In NVIDIA architectures this memory can be used to facilitate communication across threads, and it is hence referred to as shared memory. Typically, scratchpad memories are organized in multiple independently-accessible memory banks, and this SPM shares the same design principles. 
%Therefore if all memory accesses request data mapped to different banks, they can be handled in parallel. Bank conflicts occur whenever multiple requests are made for data within the same bank. If N parallel memory accesses request the same bank, the hardware serializes the memory accesses, causing an N-times slowdown. In this context, a dynamic bank remapping mechanism, based on specific kernel access pattern, may help minimize bank conflicts.
%Bank conflict reduction has been addressed by several scientific works during the last years. MANGO developed a deeply customizable scratchpad memory system for the nu+ core. At the heart of the proposed architecture is a multi-bank parallel access memory system for GPU-like processors. The proposed architecture enables a dynamic bank remapping hardware mechanism, allowing data to be redistributed across banks according to the specific access pattern of the kernel being executed, minimizing the number of conflicts and thereby improving the ultimate performance of the accelerated application.
In particular, relying on an advanced configurable crossbar, and extensive parameterization, the proposed SPM can enable highly parallel accesses matching the potential of parallelism of the GPU-like core, dictated by its SIMD structure with L multiple lanes. %All lanes share the same control unit, hence in each clock cycle they execute the same instruction, although on different data. Every time a new instruction is issued, it is propagated to all execution lanes, each taking the operands from their corresponding portion of a vectorial register file addressed by the instruction. 
The typical memory instructions provided by a SIMD ISA offer gather and scatter operations. Such operations are respectively vectorial memory load and store memory accesses. If the SIMD core has a single-bank SPM with a single memory port, the previous instructions require at least L clock cycles. This is because the L lanes cannot access a single memory port with different addresses in the same clock cycle. %The figure below shows the internal architecture of the proposed SPM.

The baseline SPM presented in this dissertation takes as input L different addresses to provides support to the scattered memory access. In the default configuration the SPM can handle up to $16$ concurrent memory transactions at once, one per hardware lane. Further details are provided in section \ref{sec:base_SPM}

\paragraph{Toolchain}
The target architecture comes with a toolchain based on the LLVM project and includes a custom version of the Clang front-end and a native nu+ back-end.
The Clang front-end allows users to compile C/C++ source code in a fast way and with a low memory usage. On the other hand, the toolchain is deeply customized for exploiting the core internal data parallelism and reaching the maximum throughput.
The compiler has a complete vision of the SIMD nature of the datapath. It supports custom vector types, thus standard arithmetic and bitwise operators are available for both scalar and vector operations.
Furthermore, the custom version of Clang supports ad-hoc built-in functions that are required to fully exploit target specific features, such as thread synchronization and special SIMD operations.  