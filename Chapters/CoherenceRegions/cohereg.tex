\chapter{Selective coherence in many-core accelerators} \label{chapt:regions}
%
%
\vskip 1cm
%
%

Modern architectures, to maximize resource and power efficiency, tend to rely on parallelism to improve performance, with multi/many-core accelerators being today commonplace. In this dissertation, we demonstrated that in such scenarios, shared memory and hardware customization are important facilities acting as a key enabler for programmer-friendly models exposed to software developers as well as for the effective adaptation of existing parallel applications. In chapter \ref{chapt:parallel}, we explored the benefit of a non-coherent scratchpad memory local to the processing element. This chapter aims to extend that concepts to suite many-cores, exploring design beneficts and tradeoffs for non-coherence in many-core systems. 

In particular, the baseline coherence sub-system, presented in chapter \ref{chapt:coherence}, and the related hardware infrastructure are augmented with a mechanism allowing coherence to be selectively deactivated, so as to match the specific application access patterns and minimize the coherence-related overhead in terms of communication and energy, a fundamental requirement for accelerator-based systems.

%The following sections provide the technical detail of the methodology and the corresponding hardware architecture. Importantly, unlike typical works dealing with coherence in homogeneous processors, the approach has not only been demonstrated in simulation. Rather, the technique has been embodied in a fully-fledged manycore accelerator, also featuring hardware multithreading and vector instructions as well as a comprehensive software toolchain, available as an RTL model and emulated on a large-scale FPGA-based platform attached to a CPU-based HPC host.
%The work also presents the results of our experimental evaluation, relying on a variety of workloads that exhibit various compute and memory access patterns. The collected results confirm the impact of the proposed technique in terms of load on the communication infrastructure and, consequently, of the measured execution time and energy consumption.

\section{Motivations} \label{sec:intro-rc}
%

%%%%%%%%%%%%%%
Common wisdom says that coherence support provided through hardware mechanism does not scale when moving to many-core architecture. Instead, tomorrow's systems will communicate with software-managed coherence, strictly coupled with scratchpad memories and message passing support. In \cite{martin2012chip}, authors firmly refuse such arguments and defend the deployment of hardware coherence in future systems, providing a scalability analysis on a on-chip coherence protocol which combines known techniques, such as share caches and explicit eviction notifications. Authors' main outcomes show that the costs on-chip coherence grow slowly with the core number, pointing out that hardware coherence is here to stay. 

%%%%%%%%%%%%%%
In a traditional Modified/Shared/Invalid-based coherence protocol (described in chapter \ref{chapt:back}), the directory tracks all blocks indistinctly even when it has a single sharer. In real systems, where the directory is set-associative and inclusive, a new transaction can arise a directory entry eviction which invalidates all the copies in all L1 caches. Moreover, if the owner is still working with the evicted block, such an invalidation generates a further memory request for the evicted block which will probably cause a new replacement in the directory, leading to a further performance loss and network flooding. This scenario is dramatically expensive in terms of messages over the network and in terms of performance. In case of private data, these operations are counter-productive work. Furthermore, the evicted data might be loaded back into the L1 unchanged, and in the meanwhile the core stays idle.

Enlarging the size of directory caches, of course, can mitigate this problem, but it negatively impacts directory access latency and area requirements. Moreover, a significant fraction of the memory blocks used by parallel applications are private, thus accessed only by one processor~\cite{hardavellas2009reactive,cuesta11}. The scenario is even worse in heterogeneous systems. Deploying hardware-based coherence support in such architectures is challenging and highly error-prone. However, selective hardware coherence can enable new classes of applications through low network-overhead and application specific grain data sharing~\cite{power2013heterogeneous}.

In this chapter, a novel hardware coherence system for heterogeneous accelerators is described, with customizable granularity and \textbf{noncoherent region} support optimization, meant to be protocol independent. This solution has been developed in hardware because future systems will continue to implement coherence support in hardware~\cite{martin2012chip}. The proposed hardware solution aims to be flexible and to significantly reduce the messaging overhead due coherence transactions. The implementation extends the traditional MSI protocol with optimizations for private data. The proposed extension keeps into account that private data require no coherence maintenance, which often induces substantial overheads at the directory level and in terms of messages flowing through the network. The baseline nu+ coherence maintenance system has been augmented in order to distinguishes private and shared data, avoiding unnecessary coherence operations and optimizing indirection latencies for private data. When a memory request to a private data occurs, the Cache Controller forwards it to the LLC, bypassing the directory controller which is totally unaware of the current transaction. This extension deeply leverages on a new state, called \texttt{U} detailed in section \ref{sec:proposed-nc}, which keeps the block alive in the L1 cache and marks it as \textbf{noncoherent}, at the same time the directory has no entry regarding that block. 

This proposed solution is oriented to modern heterogeneous many-core systems with a NoC-based communication infrastructure and strictly coupled with a sparse directory mechanism. In such architectures, optimizing network traffic, reducing the directory indirection latencies, and minimizing the directory utilization represent important goals for tomorrow's heterogeneous systems.

\section{Related Works} \label{sec:related-rc}
%
%deactivating coherence for private memory blocks~\cite{cuesta11}
Traditional coherence mechanisms operate on the same assumption: all blocks may be shared at any time, although this is an unlikely scenario in modern workloads.
%Coherence selective deactivation, directory utilization, and indirection latency reduction are main topics in many-core research.  
%
In~\cite{kelm2010cohesion}, Kelm et al. propose \texttt{Cohesion} a hybrid solution which allows switching from a hardware coherence maintenance scheme to a software model, and vice versa. Both the employed software model (presented in~\cite{kelm2009task}) and the hardware counterpart have been explored in a 1024-core homogeneous architecture~\cite{kelm2009rigel}. Their solution deeply relies on a global coherence table, placed within the LLC, which tracks the coherence approach to use and the block-level granularity. The experimental platform has a clustered multi-stage organization with a multi-layer interconnection system which allows this information to be propagated through lower level caches. Furthermore, a custom protocol allows the system to safely jump from a coherence scheme to the other, modifying the table at run-time while keeping the lower level caches in a consistent state. %On the other hand, in our work we explored a design composed of heterogeneous tiles organized in mesh topology, where a network-on-chip interconnection allows both intra-tile and inter-tile communication. Moreover, each tile has been extended with a coherence table, resulting in a reduction of the traffic caused by table queries. The synchronization infrastructure still allows different tiles to agree on a common memory layout, when needed. The design is independent of the specific software coherence protocol used.

A number of works in the literature target small/medium-scale on-chip multi-processor (CMP) systems
based on snooping coherence protocols, e.g. resorting to coarse-grain coherence to reduce the generated traffic.

In~\cite{moshovos2005regionscout} each core keeps track of sharing information with a coarse granularity, dividing the memory space into regions, where a region is defined as a contiguous memory area whose size is a power of $2$. The shared memory bus ensures that this information, albeit replicated in each core, is always kept consistent.
Since cores are aware of the exclusive ownership of blocks in a given memory region, they can reduce the number of requests that are dispatched through broadcast over the memory bus.
This results in a traffic reduction along with a cache energy saving in the tag look-up phase. However, this does not solve the false sharing problem: multiple cores accessing different part of the same memory block still compete for its ownership, generating increased coherence traffic. Furthermore, this solution extensively uses broadcasting, which scales poorly with the number of tiles.
%Since our design targets scalable heterogeneous system, the coherence protocol is directory-based, and the cores have no way to passively detect the exclusive ownership of a memory region. For this reason, the programmer must specify explicitly memory regions over which the coherence system should be turned off. On the other hand, each core stores a byte-level dirty mask for each cache block, which is evaluated when the cache line is flushed back to the main memory. Such a mechanism allows multiple cores to work on non-overlapped portions of the same memory blocks without incurring in any data loss and useless contentions.
%
Similar considerations can be made for~\cite{cantin2005improving}, where a table called \texttt{Region-Coherence Array} is deployed to track the memory region's coherence state. In contrast with~\cite{moshovos2005regionscout}, this solution contains precise information and thus requires stricter constraints on the size of the data structures.

Demetriades et al.~\cite{demetriades2014stash} propose \texttt{Stash Directory} a solution which extends the traditional sparse directory and aims to increase performance by avoiding any invalidation block from the directory that refers to a private block. In fact, in inclusive directories most of the block evictions invalidate private blocks, resulting in a dramatic performance degradation. Moreover for such blocks, coherence maintenance is unnecessary. \texttt{Stash Directory} tracks private blocks and forces the eviction of the directory entry without invalidating the corresponding cache block, which stays alive. Our solution shares the same motivation and basic observations. % although in our work directories are totally unaware of private blocks, hence both directory entry evictions and directory indirection are avoided.

Other works propose coherence protocols targeted at optimizing private blocks. In~\cite{pugsley2010swel}, the authors propose a novel protocol exploiting the fact that in many workloads a large fraction of blocks either are private to a core or are shared by different cores in read-only mode. Such blocks do not strictly require coherence information. Only shared and written blocks need to be tracked. The work aims to eliminate both the traditional coherence invalidation/update scheme and the costly sharer-tracking mechanism which limits the scalability. The authors present a new protocol, named \texttt{SWEL} after its states, which aims to overcome the directory storage overhead, the need for indirection as well as the traditional protocol complexity. This solution does not track sharer in any structure resulting in a better scalability, although it relies on a broadcast-based invalidation mechanism in order to retrieve all copies of memory blocks from the sharers, which is well known for its poor performance and network overhead. %Our approach and SWEL share the same motivation: both aim to optimize protocol indirection in order to improve performance when coherence is unnecessary, although our solution extends a traditional MSI protocol, which, in our advice, requires minor extensions on an existing solution. Moreover our solution does not rely on broadcasting which is well known for its poor performance and low network efficiency.   

Other solutions aim to reduce the directory size by redefining the granularities of the coherence regions. \texttt{SCT}~\cite{alisafaee2012spatiotemporal} supports a dual-grain coherence tracking system which tracks private regions by observing cores' memory requests and keeping only one entry at the directory level for any number of blocks in that region. %We share the same goal, namely optimizing directory utilization which often results in a size reduction, although in our work noncoherent regions are not tracked at the directory level. 

Cuesta et al.~\cite{cuesta11} aim to improve the efficient use of the memory in the directory. The authors propose a mechanism that classifies memory block, dividing them into private and shared data, and provides coherence support only for shared blocks. Both classification and coherence maintenance are done at a page granularity by the operative system, which considers every new page loaded private by default. This solution requires changes into the operative system, although no dedicated hardware is required.

Power et al. work, \texttt{Heterogeneous System Coherence}, focuses on supporting hardware coherence on CPU-GPU-centric heterogeneous systems~\cite{power2013heterogeneous}. Their study highlights coherence bottlenecks in future high-bandwidth heterogeneous systems, showing that limited directory resources are a significant bottleneck in common CPU-GPU systems. They redesigned the traditional directory in a region-based fashion and optimized the bus traffic due to coherent transactions, resulting in reduced directory congestion.

The following sections provide the technical detail of the methodology and the corresponding hardware architecture. Importantly, unlike typical works dealing with coherence in homogeneous processors, the approach has not only been demonstrated in simulation. Rather, the technique has been embodied in a fully-fledged many-core accelerator, also featuring hardware multithreading and vector instructions as well as a comprehensive software toolchain, available as an RTL model and emulated on a large-scale FPGA-based platform attached to a CPU-based HPC host.

\section{Proposed solution} \label{sec:proposed-nc}
This section discusses the designed solution, illustrating the networking infrastructure. Next, the extension to the baseline hardware coherence system and its implementation are presented, detailing the selective coherence deactivation mechanism and the extended coherence protocol for enhancing private data maintenance. 

%%%%%%%%%%% Network
\subsection{Networking Infrastructure and Synchronization Support}
The solution has been developed in the context of the heterogeneous many-core accelerators depicted in chapter \ref{chapt:core}. The baseline network-on-chip-based design composed of heterogeneous tiles organized in a 2D mesh topology, has been augmented with the distributed synchronization support exposed in chapter \ref{chapt:barrier}. 
%The router is strictly coupled with a network interface module which provides a packet-based communication over four different virtual channels, simplifying the communication among different modules on different tiles. Such networking infrastructure allows both intra-tile and inter-tile communication. 

A virtual channel is totally dedicated to service message flows, such as synchronization commands, host requests, and configuration messages. In particular, the many-core system supports a distributed synchronization mechanism based on hardware barriers, which allows accelerators in different tiles to synchronize, supporting up to $16$ concurrent synchronizations. Such a synchronization support is an essential feature when dealing with non-coherent regions. Explicit synchronization points help the different cores to gather all the partial result blending them in the final outcome. 

%%%%%%%%%%% PE
\subsection{Accelerators}
On the accelerator-level, few extensions have been made. As shown in chapter \ref{chapt:core}, the core is organized in $N$ hardware lanes (or SIMD vectors), each capable of both integer and floating-point operations on independent data. Correspondingly, each thread is equipped with a vector register file, where each register can store up to $N$ scalar data in order to satisfy the execution pipeline data throughput. Such a data parallelism allows each thread to perform SIMD operations on $N$ independent data simultaneously. In this chapter, this architecture employs $N=16$ lanes, computing $16$ operations on $32$-bit data concurrently. The accelerator supports an $n$-way set-associative write-back L1 caching system, and a load/store unit tightly interconnected with the cache controller. The load/store unit is independent of the adopted coherence protocol. For each cached block it stores two permission bits, namely \texttt{can\_read} and \texttt{can\_write}, which are updated by the cache controller and used by the accelerator to detect a cache hit or miss. 
In this configuration, the cache line width matches the internal hardware lanes capability, thus a read memory request loads $16$ scalar data from main memory and stores them into a vector register at once, matching the number of hardware lanes physically allocated. 

Finally, each core has be enhanced with a byte-level dirty mask for each private cache block, which is evaluated when the cache line is flushed back to the LLC. This mask is attached to the message and the LLC proceeds to update just the dirty part of the cached value. Such a mechanism allows multiple cores to work on non-overlapping portions of the same memory blocks without incurring any data loss and unneeded contentions, providing an effective solution to false sharing problem. The synchronization infrastructure still allows different tiles to agree on the same memory layout, when working concurrently on the same region. The design is independent of the specific software coherence protocol used. A dirty bitmask is always tracked for each noncoherent block. When the block flushes back to the LLC (which happens in the case of an explicit flush or an eviction from the L1 cache), the mask is attached to the message and the LLC proceeds to update just the dirty part of the cached value. This provides an effective solution to the false sharing problem.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.65\textwidth]{Chapters/CoherenceRegions/figures/extended.eps}
	\caption{ Detail of a processing tile in the proposed solution for heterogeneous systems. This figure highlights the Cache Controller and the extension for noncoherent region support. The CC mainly relies on the distributed directory, on an extended coherence protocol which resides in its embedded protocol ROM, and on a local bypass which allows the CC to directly access the forward virtual network interface of the network infrastructure.}
	\label{fig:ext_cc-nc}
\end{figure}

%%%%%%%%%%% Coherence sub-system
\subsection{Coherence sub-system}
This section details the novel solution (Figure \ref{fig:ext_cc-nc}), and how the baseline coherence system has been extended in each tile to support selective coherence. Each tile deploys a coherence maintenance infrastructure along with the accelerator. Since the design targets scalable heterogeneous systems, the chosen coherence protocol is directory-based: each tile provides a sparse directory model directly connected to a shared L2 cache. The L2 cache is distributed all over the mesh, implementing a sparse directory model, resulting in a dramatic area reduction compared to a full-map directory model~\cite{gupta1992reducing}. %Analyzing a memory address, the corresponding home directory can be identified, into which the coherence tracking information are stored. 
On the other hand, the cache controller handles the local core's memory requests, providing the basic support for the directory-based coherence protocol. The cache controller turns the core's basic operations, such as load and store misses, into directory-based requests over the network. Furthermore, the cache controller handles responses and forwarded requests coming from the network, updating the block state in compliance with the given coherence protocol, while the accelerator is totally unaware of it. Both the cache controller and the directory controller have been designed with flexibility in mind, and their architectures are bound to no specific coherence protocol. Each is equipped with a configurable {\em protocol ROM}, which provides a simple means for coherence protocol extensions, as it precisely describes the actions to take for each request based on the current block state.
%The accelerators feature a load/store unit whose design is independent of the adopted coherence protocol. For each cached block it stores two permission bits, CanRead and CanWrite, which are updated by the cache controller and are used by the accelerator to detect a cache hit or miss. The system is highly parametric to enhance its configurability and to provide a flexible architectural exploration framework.

Finally, for coherence maintenance a noncoherent memory region table has been added, which is local and private for each tile, hence no traffic is generated when table queries occur. The granularity of a memory region is configurable. It is set to 4 MB in the default configuration. Further details are presented in Section~\ref{sec:table}. The directories are totally unaware of private blocks, and no extension is required in the directory controller, so an existing design can be used. This architectural choice is key for future coherence sub-systems, since it can improve directory entries utilization and directory indirection avoidance. The hardware support described so far comes with an extended MSI protocol which expressly represent noncoherent regions, further described in Section~\ref{sec:ex_msi}.
%For this reason, the programmer must specify explicitly memory regions over which the coherence system should be turned off. 

\begin{figure}[t]
	\centering
	\includegraphics[width=0.65\textwidth]{Chapters/CoherenceRegions/figures/table.eps}
	\caption{Detail of the Region look-up table.}
	\label{fig:region_table}
\end{figure}

\subsection{Selective coherence deactivation} \label{sec:table}
This solution aims to deactivate coherence in a selective way when unnecessary. This is achieved by using a noncoherent memory region table, which tracks the start and end addresses of noncoherent memory regions. The number of entries and the region granularity are both parameterizable at design time.
%The addresses are stored in terms of the most significant bits, with the number of bits also being a parameter: a different region granularity can thus be chosen at design time. 
The location of the table impacts the overall system performance. A single global table (which can be distributed uniformly in the design, as in~\cite{kelm2010cohesion}) ensures that at any given time the tiles agree on a common memory layout. On the other hand, the operations which can be performed on it (updates, queries, etc.) cause additional pressure on the interconnection system, along with the additional latency required to reach a remote tile. In the proposed solution, each tile is equipped with a private table, minimizing the table access latency. Consequently, operations on the table generate no additional traffic over the network-on-chip. Using private tables might lead to unwanted behaviours: the system could transit in a inconsistent state if multiple tiles access the same memory region using a different coherence mechanism. It is the programmer's duty to avoid this scenario, either statically at compile time or at run-time using the synchronization primitives provided by the architecture.

Table access is offered to the accelerator as a control register access, as shown in Figure~\ref{fig:region_table}. Control registers are placed at the tile level and are used to offer a flexible configuration and debugging infrastructure. They are directly connected to cores, which can perform both read and write operations on them. A given register is directly mapped to the region table interface, allowing entry allocation/modification from the accelerator side during run-time.
%One of the control registers is directly mapped to the table update interface. When a write is triggered on this specific register, the written value is used to decode the table entry number and the address range. On the other hand, a dedicated table look-up interface is offered. 
Furthermore, when an accelerator requests a memory access, if the requested address lies within any of the configured noncoherent regions, the local cache controller is notified that this is a noncoherent memory access. %The table look-ups thus happen before a request reaches the coherence infrastructure.


\begin{figure}[t]
	\centering
	\includegraphics[width=0.65\textwidth]{Chapters/CoherenceRegions/figures/protocol.eps}
	\caption{ Extended MSI protocol used in the Cache Controller. Only the noncoherent states are reported.}
	\label{fig:ex_prot}
\end{figure}
\subsection{Extended MSI protocol} \label{sec:ex_msi}

The plain MSI coherence protocol has been extended to support noncoherent memory blocks. The cache controller is the only coherence actor aware of the noncoherent memory blocks: the directory controller is completely bypassed in case of noncoherent accesses and thus it allocates no entry during noncoherent transactions. The proposed protocol is depicted in Figure~\ref{fig:ex_prot} in a simplified diagram which involves both the new noncoherent states and the transient states.

The cache disabling mechanism is abstracted away from the cache controller: every request coming from the local accelerator is tagged with a coherence bit, according to the region table look-up result. When a block is in the invalid state \texttt{I}, the first access will determine which coherence mechanism will be applied on it. If the access is a noncoherent load, the read request is forwarded directly to the LLC while the block is in the transient state \texttt{IUd} waiting for the data. The noncoherent state, namely \texttt{U}, is applied to this block when data are received. All the subsequent reads or writes will always result in a cache hit and no additional traffic is required to track the block status.
%
On the other hand, if the first access is a noncoherent store, the request always results in a hit, the block transits into the noncoherent write state, namely \texttt{UW}, and no memory block is fetched form the LLC. In such cases, a bit-mask is used to keep track of which bytes of the block are dirty: further loads to that parts will result in a hit. This approach comes from the observation that store-first noncoherent blocks are usually used to track either the output of the computations or the memory stack of a core (which is also private), so there is no need to fetch their previous value. This significantly reduces the overall network traffic, since no message is generated during these operations. If a noncoherent load request occurs for a block in the \texttt{UW} state, the cache controller checks the dirty bit-mask. If the request address offset is marked as dirty, the data is retrieved from the local cache and no coherence state transition happens. On the other hand, if the requested data is not marked as dirty, it needs to be fetched from the LLC, so the cache controller sends the request over the network-on-chip and moves the block state from \texttt{UW} to the transient state \texttt{UWUd}. As soon as the data is received, the block transits into state \texttt{U}.

%Extending a traditional protocol, in our advice, requires minor is less error-prone, the outcome is more reliable when based on an existing solution.
Notice that solution does not rely on broadcasting for blocks eviction, thereby improving performance and network efficiency.


\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/CoherenceRegions/figures/results2.eps}
	\caption{a) Total number of FLITs flowing through the network-on-chip.  b) Dynamic power consumption of the networking infrastructure.}
	\label{fig:flits}
	\label{fig:power}
\end{figure}

\section{Experimental evaluation} \label{sectionLabel3-ref}
The evaluation reported in this section has been performed on a $4\times 4$ mesh featuring $14$ accelerators uniformly distributed over the mesh network, a memory controller tile and a host-communication tile. Each accelerator deploys $8$ hardware threads and $16$ hardware lanes. Threads in the same accelerator share the same L1 cache and network access interface. The numerical evaluations are carried out on a proFPGA MB-4M FPGA board by ProDesign, equipped with one Xilinx Virtex-7 2000T XC7V2000T FPGA.

The performance of the system has been evaluated on a set of common parallel workloads. The output computations have been uniformly spread among $8$ accelerators that perform the same computations occurring in the same number of memory accesses. When a thread hits the end of the kernel, it signals to the host-communication tile the kernel termination. When all these messages from all the running cores are gathered by the host-communication tile, the kernel is finished and the results are ready in the main memory. Each workload is executed in both modes: (1) using a traditional MSI coherence support, and (2) employing the proposed optimizations. In the latter configuration, the noncoherent regions overlap with the kernel input and output data. 

First, the impact of the proposed solution has been measured on the network traffic in terms of the total number of FLITs processed by the routers. Figure~\ref{fig:flits}a shows the results. By overlapping the noncoherent regions with the kernel data section, we observe a remarkable saving in terms of FLITs flowing over the network (up to $77\%$ less in the \texttt{convolution} workload). This is due to the lower number of unnecessary coherence requests sent to the involved directory controllers, resulting in both a considerable reduction of the indirection latencies and limited transactions overhead caused by false sharing.

\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/CoherenceRegions/figures/results1.eps}
	\caption{a) Number of cycles for each kernel in coherent and non-coherent configurations. b) Total number of data misses of the whole system, with 8 accelerators.}
	\label{fig:cycles}
	\label{fig:misses}
\end{figure}

Then, an evaluation of the impact of the proposed mechanism at the core level has been conducted in terms of total kernel cycles required for the workload completion, and the number of data misses occurred during the computation. These results are shown in Figure~\ref{fig:misses}b and Figure~\ref{fig:cycles}a. The number of data cache misses drops in all the presented workloads, almost up to $80\%$ in the \texttt{dct}, and about $85\%$ in the \texttt{kmeans} workload, while in \texttt{convolution}, \texttt{matrix multiplication}, and \texttt{marching squares} these numbers stay constant. Furthermore, in \texttt{dct}, \texttt{fft}, and \texttt{kmeans} these reductions are more prominent due to the distribution of the data. Such kernels are deeply affected by false sharing due the granularity of the memory blocks. In these cases, different threads concur for different data placed within the same memory blocks, resulting in coherence maintenance which generates unnecessary network messages when running with plain MSI coherence support. In terms of kernel duration, a reduction can be observed in most of the presented workloads, up to $25\%$ for \texttt{matrix transpose}. In three cases, the proposed solution has a marginal impact, namely \texttt{convolution}, \texttt{fft}, and \texttt{marching squares}. Although both workloads generate less FLITs, the memory layout for noncoherent  data, and the accelerators' multithreading support hide the potential improvement. 

Finally, the dynamic power reduction has been evaluated at the networking infrastructure level. Figure~\ref{fig:power}b shows the results. All explored workloads experience a power reduction, up to $5\%$ less in the \texttt{matrix transpose} workload case. These results are directly correlated to the previously exposed results, namely the reduction of flits and directory activity. 

\begin{table}[t]
\centering
\begin{tabular}{cccc}
	%\hline\hline
	\multicolumn{4}{c}{\textbf{Hardware Implementation Overhead}}\\[0.1cm] \hline
	\multicolumn{1}{c||}{\textbf{Solutions}}    & \textbf{LUT} & \textbf{Flip-Flop} & \textbf{BRAM} \\[0.1cm] \hline
	\multicolumn{1}{r||}{\textbf{proposed}}     & 24045           & 46308          & 0             \\[0.1cm]
	\multicolumn{1}{r||}{\textbf{plain}}        & 20888           & 43197          & 0             \\[0.1cm]
	\hline \hline
\end{tabular}
\caption{Resources occupation on a Virtex-7 2000T XC7V2000T FPGA, in term of LUTs, FFs, and BRAMs.}
\label{tab:evalarea1}
\end{table}

%\begin{table}[]
%\begin{tabular}{|c|c|c|c|c|c|c|}
%Area & DC LUT   & DC FF    & L1D LUT  & L1D FF   & CR LUT   & CR FF \\
%new  & 4815     & 7305     & 18643    & 38307    & 587      & 696   \\
%old  & 4815     & 7305     & 15521    & 35364    & 552      & 528
%\end{tabular}
%\end{table}

\subsection{Implementation Overhead}
This section compares the hardware overhead per tile in the new coherence maintenance solution versus a plain version which provides no support for either multi-grain blocks, selective coherence deactivation, or private data optimizations. In both cases the miss status holding register (MSHR) in the Cache Controller and the transaction status holding register (TSHR) in the Directory support $32$ pending transactions, while the Region Table has $128$ entries. Table~\ref{tab:evalarea1} shows the occupation values on the Virtex-7 2000T XC7V2000T FPGA in term of LUTs, FFs, and BRAMs. The novel coherence maintenance system incurs an overhead of $13\%$ in terms of LUTs and $6\%$ for the FFs compared to the plain counterpart.

\section{Conclusions} \label{sec:conclusions-rc}
This chapter described a novel and scalable architectural solution which selectively supports noncoherent regions for heterogeneous NoC-based systems. In particular, it evaluated how the proposed solution can impact the performance of common parallel workloads. Experimental results showed that the use of a hybrid coherent and noncoherent architectural mechanism along with an extended coherence protocol can enhance performance. This combination lowers the overall network-on-chip traffic of a $4\times 4$ mesh, and significantly decreases cache misses. Furthermore, the kernel duration is positively affected in most workloads along with the overall dynamic power consumption of the networking subsystem. In conclusions, the findings of our work may be particularly impactful for driving the long-term evolution of current heterogeneous architectures, especially NoC-based manycore accelerators, towards improved specialization and workload-specific customizability.