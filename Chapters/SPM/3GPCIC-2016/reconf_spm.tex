%

%The core is fully synthetizable on FPGA with a contained hardware cost. We also validated the presented architecture with a cycle-accurate event-driven emulator written in C++ as well as an RTL simulator tool. Last, we demonstrated the impact of bank remapping and other parameters available with the proposed configurable shared scratchpad memory by evaluating the performance of two real-world parallelized kernels.

Following the results of the previous exploration, the current Chapter focuses on non-coherent scratchpad memories, studying their impact and proposing a novel solution for reducing banks conflict which highly affects performance in these type of memories. The baseline SPM, hereafter detailed, has been extended with a hardware remapping mechanism and fully integrated at core level, providing a first proof of extendibility of the nu+ platform.

\section{Motivations} \label{sec:mot-spm}

This dissertation strongly focuses on  architectural customization which can play a key role in future complex architectures, as it enables unprecedented levels of power-efficiency compared to CPUs/GPUs. This is the essential reason while very recent trends are putting more emphasis on the potential role of FPGAs, beyond very special-purpose acceleration. On the other hand, recent FPGA families, such as the Xilinx Virtex-7 or the Altera Stratix 5, have innovative features, providing significantly reduced power, high speed, lower materials cost, and reconfigurability~\cite{fpgavsasic}. Due to these changes, in the very recent years many innovative companies, including Convey, Maxeler, SRC, Nimbix~\cite{paranjape2010heterogeneous}, have introduced FPGA-based heterogeneous platforms used in a large range of HPC applications, e.g. multimedia, bioinformatics, security-related processing, etc.~\cite{sarkar10,paranjape2010heterogeneous,cilardoTIFS13-ref}, with speedups in the range of 10x to 100x.

This Chapter explores the adoption of a deeply customizable non-coherent scratchpad memory system for FPGA-oriented accelerator designs. At the heart of the proposed architecture is a multi-bank parallel access memory system developed on the baseline GPU-like processors described in chapter \ref{chapt:core}. The baseline SPM has been enhanced with a dynamic bank remapping hardware mechanism, allowing data to be redistributed across banks according to the custom access pattern of the kernel being executed, miminizing the number of conflicts and thereby improving the ultimate performance of the accelerated application. 

In particular, relying on an advanced configurable crossbar, on a hardware-supported remapping mechanism and extensive parameterization, the proposed architecture can enable highly parallel accesses matching the potential of current HPC-oriented FPGA technologies.
The remainder of the Chapter describes the main insights and innovations enabled by dynamic bank remapping mechanism as well as the key role that hardware customization along with selective non-coherent solutions, such as scratchpad memories, might play for tomorrow's high-performance computing applications.

\section{Related Works} \label{sec:related-spm}
%%%%%%%%%%%%%%%%%%%%%
%Shared Memory
%%%%%%%%%%%%%%%%%%%%%

As in standard platforms, in GPU-like processors data movement and memory access is particularly critical for performance. Cache hierarchy has been the traditional way to alleviate the memory bottleneck. However, cache coherence mechanisms are complex and not needed in some applications. Many modern parallel architectures utilize fast non-coherent user-managed on-chip memories, also known as \textbf{scratchpad memories}. Existing open source GPU-like core projects provide limited hardware support for shared scratchpad memory and particularly for the problem of bank conflicts, a major source of performance loss with many parallel kernels. 
%
Since NVIDIA Fermi family~\cite{fermi}, commercial GPUs are equipped with this kind of memories. In NVIDIA architectures this memory can be used to facilitate communication across threads, in this case is also referred as shared memory. Typically, scratchpad memories are organized in multiple independently-accessible memory banks. Therefore, if all memory accesses request data mapped to different banks, they can be handled in parallel. Bank conflicts occur whenever multiple requests are made for data within the same bank~\cite{faber}. If $N$ parallel memory accesses request the same bank, the hardware serializes the memory accesses, causing an $N$-times slowdown~\cite{coon-brett-nvidia}. In this context, a dynamic bank remapping mechanism, based on specific kernel access pattern, may help minimize bank conflicts.

Bank conflict reduction has been addressed by several scientific works during the last years. A generalized memory-partitioning (GPM) framework to provide high data throughput of on-chip memories using a polyhedral mode is proposed in~\cite{wang-yuxin}. GPM allows intra-bank offset generation in order to reduce bank conflicts. Memory partitioning adopted in these works are cyclic partitioning and block partitioning, as presented in~\cite{chatterjee}.

In~\cite{cilardo-gallo} the authors address the problem of automated memory partitioning providing the opportunity of customizing the memory architecture based on the application access patterns and the bank mapping problem with a lattice-based approach.

While bank conflicts in shared memory is a significant problem, existing GPU-like accelerators~\cite{nyuzi, miaow, guppy, kingyens} lack bank remapping mechanisms to minimize such conflicts.

\section{Architecture} \label{sec:base_SPM}
The following sections describe the baseline scratchpad memory design, its interface with the GPU-like core, and how it has been extended with the dynamic bank remapping mechanism.

\begin{figure}[t]
    \centering
    \includegraphics[scale=0.55]{Chapters/SPM/3GPCIC-2016/figures/SPMContext.eps}
    \caption{High-level generic GPU-like core with scratchpad memory.}
    \label{fig:int}
\end{figure}

\subsection{SPM interface and operations}

Figure~\ref{fig:int} depicts a block diagram of the SPM in the context of the baseline GPU-like core architecture of chapter \ref{chapt:core}. The GPU-like core has a SIMD structure with $L$ multiple hardware lanes. These lanes share the same control unit, hence in each clock cycle they execute the same instruction, although on different data.
Every time a new instruction is issued, it is propagated to all execution lanes, each taking the operands from their corresponding portion of a vectorial register file addressed by the instruction.
The typical memory instructions provided by a SIMD ISA offer gather and scatter operations. Such operations are respectively vectorial memory load and store memory accesses. If the SIMD core has a single-bank SPM with a single memory port, the previous instructions require al least $L$ clock cycles. This is because the $L$ lanes cannot access a single memory port with different addresses in the same clock cycle.

\begin{figure}[t]
	\centering
	% Use the relevant command for your figure-insertion program
	% to insert the figure file.
	% For example, with the graphicx style use
	\includegraphics[scale=.65]{Chapters/SPM/3GPCIC-2016/figures/Architecture.eps}
	%
	% If no graphics program available, insert a blank space i.e. use
	%\picplace{5cm}{2cm} % Give the correct figure height and width in cm
	%
	\caption{SPM design overview.}
	\label{fig:arc}       % Give a unique label
\end{figure}

The baseline SPM, depicted in Figure \ref{fig:arc}, can be regarded as an FSM with two states: \texttt{Ready} and \texttt{Busy}. In the \texttt{Ready} state, the SPM is ready to accept new memory requests. In the \texttt{Busy} state, the SPM cannot accept any request as it is still processing the previous one, so in this state all input requests will be ignored. The Address Mapping Unit computes in parallel the bank index and the bank offset for each of the L memory addresses coming from the processor lanes. Bank index in the figure denotes the index of the bank to which the address is mapped. Bank offset is the address of the word into the bank. The Address Mapping Unit behaviour can be changed at run time in order to change the relationship between addresses and banks. This is a key feature in that it allows the adoption of the mapping strategy that best suits the executed workload. The Serialization Logic Unit performs the conflict detection and the serialization of the conflicting requests. Whenever an n-way conflict is detected, the Serialization Logic Unit puts the SPM in the busy state and splits the requests into n conflict-free requests issued serially in the next n clock cycles. When the last request is issued, the Serialization Logic Unit put the SPM in the ready state. Notice that for the Serialization Logic Unit, multiple accesses to the same address are not seen as a conflict, as in this occurrence a broadcast mechanism is activated. This broadcast mechanism provides an efficient way to satisfy multiple load requests for the same constant parameters.
The Input Interconnect is an interconnection network that steers source data and/or control signals coming from a lane in the GPU-like processor to the destination bank. Because the Input Interconnect follows the Serialization Logic Unit, it only accepts one request per bank. Then, there are the B memory banks providing the required memory elements. Each memory bank receives the bank offset, the source data, and the control signal from the lane that addressed it. Each bank has a single read/write port with a byte-level write enable signal to support instructions with operand sizes smaller than word. Furthermore, each lane controls a bit in an L-bit mask bus that is propagated through the Input Interconnect to the appropriate bank. This bit acts as a bank enable signal. In this way, we can disable some lanes and execute operations on a vector smaller than L elements.
The Output Interconnect propagates the loaded data to the lane that requested it. Last, there is a Collector Unit which is a set of L registers that collect the results coming from the serialized requests outputting them as a single vector.

%\subsection{Proposed Solution}
%%
%Many modern parallel architectures utilize such fast non-coherent user-managed on-chip memories. In NVIDIA architectures this memory can be used to facilitate communication across threads, and it is hence referred to as shared memory. Typically, scratchpad memories are organized in multiple independently-accessible memory banks. Therefore if all memory accesses request data mapped to different banks, they can be handled in parallel. Bank conflicts occur whenever multiple requests are made for data within the same bank. If N parallel memory accesses request the same bank, the hardware serializes the memory accesses, causing an N-times slowdown. In this context, a dynamic bank remapping mechanism, based on specific kernel access pattern, may help minimize bank conflicts.
%Bank conflict reduction has been addressed by several scientific works during the last years. At the heart of the proposed architecture is a multi-bank parallel access memory system for GPU-like processors. The proposed architecture enables a dynamic bank remapping hardware mechanism, allowing data to be redistributed across banks according to the specific access pattern of the kernel being executed, minimizing the number of conflicts and thereby improving the ultimate performance of the accelerated application.
%In particular, relying on an advanced configurable crossbar, on a hardware-supported remapping mechanism, and extensive parameterization, the proposed architecture can enable highly parallel accesses matching the potential of current HPC-oriented FPGA technologies.
%
%Figure~\ref{fig:arc} shows the internal architecture of the proposed SPM.
%The SPM takes as input $L$ different addresses to provides support to the scattered memory access. It can be regarded as an FSM with two states: \texttt{Ready} and \texttt{Busy}. In the \texttt{Ready} state, the SPM is ready to accept a new memory request. In the \texttt{Busy} state, the SPM cannot accept any request as it is still processing the previous one, so in this state all input requests will be ignored. The Address Mapping Unit computes in parallel the bank index and the bank offset for each of the $L$ memory addresses coming from the processor lanes. Bank index (BI$_i$ in Figure \ref{fig:arc}) is the index of the bank to which the address is mapped. Bank offset (BO$_i$ in Figure \ref{fig:arc})  is the address of the word into the bank. The Address Mapping Unit behaviour can be changed at run time in order to change the relationship between addresses and banks. This is a key feature in that it allows the adoption of the mapping strategy that best suits the executed workload.
%
%The Serialization Logic Unit performs the conflict detection and the serialization of the conflicting requests. Whenever an $n$-way conflict is detected, the Serialization Logic Unit puts the SPM in the \texttt{Busy} state and splits the requests into $n$ conflict-free requests issued serially in the next $n$ clock cycles.
%When the last request is issued, the Serialization Logic Unit put the SPM in the \texttt{Ready} state. Notice that for the Serialization Logic Unit, multiple accesses to the same address are not seen as a conflict, as in this occurrence a broadcast mechanism is activated.
%This broadcast mechanism provides an efficient way to satisfy multiple load requests for the same constant parameters. 
%
%The Input Interconnect is an interconnection network that steers source data and/or control signals coming from a lane in the GPU-like processor to the destination bank. Because the Input Interconnect follows the Serialization Logic Unit, it only accepts one request per bank. Then, there are the $B$ memory banks providing the required memory elements. Each memory bank receives the bank offset, the source data, and the control signal form the lane that addressed it.
%Each bank has a single read/write port with a byte-level write enable signal to support instructions with operand sizes smaller than word. Furthermore, each lane controls a bit in an $L$-bit mask bus that is propagated through the Input Interconnect to the appropriate bank. This bit acts as a bank enable signal. In this way, we can disable some lanes and execute operations on a vector smaller than $L$ elements.
%
%The Output Interconnect propagates the loaded data to the lane that requested it.
%Last, there is a Collector Unit which is a set of $L$ registers that collect the results
%coming from the serialized requests outputting them as a single vector.

\subsection{Remapping}

\begin{figure}[t]
\centering
% Use the relevant command for your figure-insertion program
% to insert the figure file.
% For example, with the graphicx style use
\includegraphics[scale=1]{Chapters/SPM/3GPCIC-2016/figures/Mapping.eps}
%
% If no graphics program available, insert a blank space i.e. use
%\picplace{5cm}{2cm} % Give the correct figure height and width in cm
%
\caption{This figure shows how addresses are mapped onto the banks. Takes into account that the memory is byte addressable and that each word is four byte. In the case of generalized cyclic mapping the remapping factor is 1.}
\label{fig:remap}       % Give a unique label
\end{figure}

As mentioned above, the mapping between addresses and banks can be changed at run time through the Address Mapping Unit. The technical literature presents essentially three mapping strategies: cyclic and block mapping~\cite{chatterjee, wang-yuxin}. These strategies are summarized in Figure~\ref{fig:remap}.

Cyclic mapping assigns consecutive words to adjacent banks (Bank $B-1$ is adjacent to Bank $0$). Block mapping maps consecutive words onto consecutive lines of the same banks.
The block-cycle mapping is a hybrid strategy. With $B=2^b$ banks, $W=2^w$ bytes in a word, and $D=2^d$ words in a single bank, a scratchpad memory address is made of $w+b+d$ bits.
Figure~\ref{fig:remap} shows a cycling remapping, which can be easily obtained by repartitioning the memory address.
The Address Mapping Unit described in this work implements a generalization of cyclic mapping, which we call {\em generalized-cyclic mapping}.
By adopting this strategy, many kernels generating conflicts with cyclic mapping, change their pattern by accessing data on the memory diagonal, thereby reducing the number of conflicts.

\subsection{Implementation details}

\begin{figure}[ht]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/SPM/3GPCIC-2016/figures/results1.eps}
	\caption{LUTs and FFs occupation of the FPGA-based SPM design for a variable number of banks}
	\label{fig:brans_grows}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=1\textwidth]{Chapters/SPM/3GPCIC-2016/figures/results2.eps}
	\caption{LUTs and FFs occupation of the FPGA-based SPM design for a variable number of lanes}
	\label{fig:lanes_grows}
\end{figure}

The proposed configurable scratchpad memory was described in HDL and synthesized for a Xilinx FPGA device. In particular, the design has been implemented using Xilinx Vivado on a Xilinx Virtex7-2000t FPGA (part number xc7v2000tflg1925-2). %We built our architecture with a variable number of banks and lanes.
%
Figure~\ref{fig:brans_grows} reports the SPM occupation in terms of LUTs and Flip-Flops (FFs) for a variable number of banks. On the other hand, Figure~\ref{fig:lanes_grows} reports the SPM occupation in terms of LUTs and Flip-Flops (FFs) for a variable number of lanes. Increasing the number of banks heavily affects LUTs occupation, while the number of lanes mostly affects the FF count.

The proposed SPM has been validated with the \texttt{Verilator RTL} simulator tool~\cite{verilator}. It compiles synthesizable Verilog, SystemVerilog, and Synthesis assertions into a C++ behavioural model (called the {\em verilated} model), effectively converting RTL design validation into C++ program validation.
In addition, a cycle accurate event-driven emulator written in C++ was developed for verifying the proposed design. The SPM verilated model and the SPM emulator are integrated in a testbench platform, that provides the same inputs to the emulator and the verilated model.
The test platform realized compares the outputs from the two models at every simulation cycle, checking if the verilated model and the emulator generate the same responses. Notice that the \texttt{Verilator} tool supports the HDL code coverage analysis feature, which helped us create a test suite with full coverage of the SystemVerilog code.

\subsection{Integration consideration in the baseline GPU-like core}

The presented SPM, as previous explained, has a variable and unpredictable latency, this feature can be a potential issue in GPU-like architecture integration. At issue time, before the operand fetch, the control unit is unaware of the effective latency of a scratchpad memory instruction and can not detect possible Writeback structural hazard. To avoid this problem, the core supports a dynamic on-the-fly structural hazard handler at Writeback stage, exhaustively described in Section \ref{sec:wb-core}. 


\section{Evaluation}

The experimental evaluation was essentially meant to demonstrate to which extent the amount of bank conflicts can be reduced by changing the parameters in the proposed configurable scratchpad memory. In particular, to this end, the following experiments assess how simultaneous memory accesses, as well as the bank remapping feature may affect the total bank conflict count.

\subsection{Methodology}
As first step, few kernels have been identified that have potentially highly parallel memory accessed and that might benefit from the non-coherent memory support. For example, there are many collection of benchmarks such as PolyBench~\cite{polybench}.

Next, each of those kernels are rewrote to increase the kernel memory access parallelism, as this chapter aim was to study how conflicts vary with a variable number of parallel memory requests, and how a non-coherent memory can impact on performances.

Then, the access patterns for each kernel has been studied and extracted, running them on the developed scratchpad emulator. Emulator cycle accuracy grants a perfectly emulate timings per-cycle accesses. With the same inputs, the scratchpad memory and its emulator evolve in the same lock-step fashion. 

Last, the emulator response, in terms of total bank conflict, are collected for all the memory accesses issued by the kernel, through a counter that increments when a bank conflict occurs. This experiment has been repeated for different remapping functions identified for the specific kernel as well as for a variable number of banks.

\subsection{Kernels}

\subsubsection{Matrix Multiplication}

Square matrix-matrix multiplication is a classic bank conflict sensitive algorithm. In this benchmark, I evaluated the square matrix access patterns and how the configurable parameters influence the scratchpad bank conflict count.

\begin{figure}[!h]
	\begin{lstlisting}[caption=Matrix Multiplication parallelized on hardware lane number., label=code:matrix, frame=lines, language=C, basicstyle=\scriptsize]
for (int i = 0; i < DIM; ++i)
  for (int j = 0; j < DIM; ++j)
    for (int k = 0; k < DIM/numLane; ++k)
      for (int lane = 0; lane < numLane; lane++){
	    accessA[index][lane] = (i*DIM + k*numLane + lane)*4;
		accessB[index][lane] = ((k*numLane + lane)*DIM + j)*4;
	  }
	  index++;
	\end{lstlisting}
\end{figure}

The code has been rewrote so as to maximize the exploitation of the available number of lanes in the target model of GPU-like processor.
The inner cycle, shown in Listing~\ref{code:matrix}, calculates which memory address will be accessed by each lane for both matrices.
Experiments are run with a fixed square matrix size $DIM = 128$. The number of hardware lanes is $numLane = [4, 8, 16, 32]$ while the number of banks is $numBanks = [16, 32, 64, 128, 256, 512, 1024]$. The function bank remapping is $(Entry \cdot c + Bank) \newline \bmod (NUMBANK)$ with $c = [1, 2, 4, 8, 16]$.

\begin{table}[t]
	\centering
		\footnotesize
	\resizebox{0.7\columnwidth}{!}{
	\begin{tabular}{ c  c  c  c  c  c  c}
		\toprule
		\multirow{2}*{Lanes} & \multirow{2}*{Banks} &    \multicolumn{5}{c}{Remapping factor}    \\
		\cmidrule{3 - 7}    &                       & No Remap      & 1      & 2      & 4      & 8      \\ \midrule
		\multirow{7}*{4}    & 16                    & 262146 & 131072 & 262146 & 262146 & 262146 \\
		                    & 32                    & 262146 & 0      & 0      & 131072 & 262146 \\
		                    & 64                    & 262146 & 0      & 0      & 0      & 0      \\
		                    & 128                   & 262146 & 0      & 0      & 0      & 0      \\
		                    & 256                   & 131072 & 0      & 0      & 0      & 0      \\
		                    & 512                   & 0      & 0      & 0      & 0      & 0      \\
		                    & 1024                  & 0      & 0      & 0      & 0      & 0      \\ \midrule
		\multirow{7}*{8}    & 16                    & 183505 & 131073 & 183505 & 183505 & 183505 \\
		                    & 32                    & 183505 & 0      & 65536  & 131073 & 183505 \\
		                    & 64                    & 183505 & 0      & 0      & 0      & 65536  \\
		                    & 128                   & 183505 & 0      & 0      & 0      & 0      \\
		                    & 256                   & 131073 & 0      & 0      & 0      & 0      \\
		                    & 512                   & 65536  & 0      & 0      & 0      & 0      \\
		                    & 1024                  & 0      & 0      & 0      & 0      & 0      \\ \midrule
		\multirow{7}*{16}   & 16                    & 109230 & 91756  & 109230 & 109230 & 109230 \\
		                    & 32                    & 109230 & 32768  & 65538  & 91756  & 109230 \\
		                    & 64                    & 109230 & 0      & 0      & 32768  & 65538  \\
		                    & 128                   & 109230 & 0      & 0      & 0      & 0      \\
		                    & 256                   & 91756  & 0      & 0      & 0      & 0      \\
		                    & 512                   & 65538  & 0      & 0      & 0      & 0      \\
		                    & 1024                  & 32768  & 0      & 0      & 0      & 0      \\ \midrule
		\multirow{7}*{32}   & 16                    & 61696  & 58256  & 61696  & 61696  & 61696  \\
		                    & 32                    & 59768  & 32769  & 45878  & 54615  & 59768  \\
		                    & 64                    & 59768  & 0      & 16384  & 32769  & 45878  \\
		                    & 128                   & 59768  & 0      & 0      & 0      & 16384  \\
		                    & 256                   & 54615  & 0      & 0      & 0      & 0      \\
		                    & 512                   & 45878  & 0      & 0      & 0      & 0      \\
		                    & 1024                  & 32769  & 0      & 0      & 0      & 0      \\ \bottomrule
	\end{tabular}
	}
	\caption{Matrix Multiplication results.}
	\label{tab:matrix}
\end{table}


The total SPM size is kept constant at $BANK number \times \text{ENTRY per Bank} = 2 \times DIM^2 $, so that SPM can store both matrices completely.
Results in Table~\ref{tab:matrix} show that bank remapping has a greater impact than the other parameters.
A remapping coefficient $c = 1$ drastically reduces bank conflicts, even with a limited number of banks, while adding little resource overhead compared to a solution relying on a large number of parallel banks.

\subsubsection{Image Mean Filter $5\times 5$}

Mean filtering is a simple kernel to implement image smoothing. It is used to reduce noise in images. The filter replaces each pixel value in an image with the mean value of its neighbors, including itself. In this study a $5\times 5$ square kernel is used.

\begin{figure}[!h]
	\begin{lstlisting}[caption=Image Mean Filter 5x5., label=code:mean, frame=lines, language=C, basicstyle=\scriptsize]
#define OFFSET(x, y) (((x)*DIM + y)*4)
	
for (int i = 2; i < DIM - 3; ++i)
  for (int j = 2; j < DIM - 3; ++j) {
    for (int w1 = -W1; w1 <=W1; w1++ ){
      for (int w2 = -W2; w2 <= W2; w2++){
        a = baseA.getAddress() + OFFSET(i+w1, j+w2);
        l = (w1 + 2)*5 + (w2 + 2);
        accessA[index][l] = a;
      }
    }
	index++;
  }
	\end{lstlisting}
\end{figure}

Listing~\ref{code:mean} shows the parallelized version of the mean filter. This kernel have a fixed square matrix size $DIM = 128$ and a fixed number of lanes $numLane = 30$.
The total scratchpad memory is kept constant at $BANK number \times ENTRY per Bank = DIM^2$. Then, we evaluated the bank conflicts for a variable number of banks and for two bank remapping functions: no remap and $(Entry\cdot 5 + Bank) \bmod(NUMBANK)$.
The results are shown in Table~\ref{tab:mean}. As in the case of the matrix multiplication kernel, the remapping function has the largest impact on the bank conflict count.

\begin{table}[t]
	\small
	\centering	
	\begin{tabular}{c  c  c }\toprule
		Banks & No Remap & Remap  	\\
		\midrule
		16  	& 7565 	 & 1722  	    \\
		32  	& 7565 	 & 0			\\
		64  	& 7565 	 & 0			\\
		128 	& 7565 	 & 0			\\
		256 	& 0 	 & 0			\\
		512 	& 0		 & 0		    \\
		1024	& 0		 & 0			\\
		\bottomrule
	\end{tabular}
	\caption{Image Mean Filter 5x5.}
	\label{tab:mean}
\end{table}


\section{Conclusion}
This Chapter presented a configurable GPU-like oriented scratchpad memory with bank remapping supports, fully synthetizable on FPGAs. Various architectural aspects like the number of banks, the number of lanes, the bank remapping function, and the size of the total memory are parameterized.
Reconfigurability helped explore architectural choices and assess their impact. This Chapter described the SPM design in HDL and extensively validated it. A software cycle accurate and event-driven emulator of our SPM component has been also developed to support the experimental evaluation with real code. 

Through two case studies, a matrix multiplication and a $5 \times 5$ image mean filter, the experimental results showed the performance implications with different configurations and demonstrated the benefits of both using a customizable hardware bank remapping function over other architectural parameters and non-coherent memories for some kind of algorithms.

%As a long-term goal of this research, we aim to integrate our SPM architecture in an open source GPU-like core, enabling it to take full advantage of the underlying FPGA technologies.