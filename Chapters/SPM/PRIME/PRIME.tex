\section{Motivations} \label{intro-sec}

An ever increasing number of challenging applications are being approached using Deep Learning, obtaining impressive results in a variety of different domains. However, state-of-the-art accuracy requires deep neural networks with a larger number of layers and a huge number of different filters with millions of weights. GPU- and FPGA-based architectures have been proposed as a possible solution for facing this enormous demand of computing resources. This class of algorithms well suit both thread-level and SIMD parallelization, making them a perfect starting point for an exploration study. 

\section{Related Works} 
The emerging wave of the Big Data~\cite{kambatla2014trends} is paving the way for the widespread adoption of Deep Learning techniques in diverse application domains including image recognition~\cite{he2016deep}, sound processing~\cite{salamon2017deep}, medical systems~\cite{esteva2017dermatologist}, gaming~\cite{silver2016mastering}, and others. 
%
However, despite the huge potential of Deep Learning, most of these algorithms rely on a large number of performance-hungry convolutions limiting the usability of these techniques. In addition, Deep Neural Networks (DNNs) require a training phase that is a very compute intensive task. For instance, training a popular architecture like, e.g. GoogLeNet~\cite{szegedy2015going}, can easily take several days on a standard GPU. Because of these requirements, the applicability of Deep Learning is becoming increasingly performance- or power-constrained. 

Not surprisingly, the industry and academia are continuously introducing new architectures dictating the evolution of Deep Learning techniques. First-generation solutions consist of large-scale distributed systems comprised of tens of thousands of CPU cores~\cite{dean2012large}. However, the growing demand for high-parallel energy-efficient architectures has led to an increasing interest in GPUs and FPGAs~\cite{chen2014dadiannao,farabet2011neuflow,zhang2015optimizing}. For example, many entries in the annual ImageNet Large Scale Visual Recognition Challenge (ILSVRC)~\cite{russakovsky2015imagenet} use GPUs and FPGAs to implement DNNs. Custom accelerators and FPGAs are an attractive alternative and provide an intermediate point between Application-Specific Integrated Circuits (ASIC) and standard GPUs, enabling higher efficiency even compared to high-end GPUs~\cite{murphy2017xilinx}. In addition, the higher flexibility allows their use in different compute problems. There is thus a tradeoff between power-hungry high-performance GPUs and energy-efficient application-specific solutions. 

%In this paper we investigate the adoption of different architectural features, i.e. SIMD paradigm, multithreading, and non-coherent on-chip memory for Deep Learning oriented FPGA-based accelerator designs. We designed and implemented a customizable GPU-like SIMD architecture as a solution to support architecture-level exploration for Deep Learning oriented systems. Architectural customization plays a key role, as it enables unprecedented levels of resource-efficiency compared to GPUs. The accelerator was synthesized into a Xilinx Virtex-7 2000T XC7V2000T FPGA chip. Experimental results show that such a customization leads to significant improvements over non-customized architectures.

%In addition, the major global industrial players (e.g. Google, IBM, NVIDIA) have brought to the market specialized compute nodes providing significant benefits for Deep Learning applications. 

%\section{GPU-like architecture} \label{arch-ref}
%\input{Chapters/SPM/PRIME/Architecture/architecture.tex}

%\section{Convolution algorithm} \label{alg-ref}
\section{Convolutional Layer} \label{alg-ref}
\input{Chapters/SPM/PRIME/Convolution/conv.tex}

\section{Evaluation} \label{results-sec}
%
The following experiments are carried out on a proFPGA MB-4M FPGA board by ProDesign, equipped with one Xilinx Virtex-7 2000T XC7V2000T FPGA. %The GPU-like accelerator, described in Chapter \ref{chapt:core}, has been developed in SystemVerilog hardware description language (HDL) and synthesized using the Vivado design suite provided by Xilinx. The design has been validated with the Verilator RTL simulator tool~\cite{snyder2012verilator} and with an in-house event-driven cycle-accurate emulator. 
%Finally, we built from scratch a LLVM backend and a custom version of the Clang frontend. This provides most of a toolchain, to allow replacing the full GCC stack. 
The convolution algorithm showed above was written in {\em C} and run on the baseline GPU-like accelerator described in Chapter \ref{chapt:core}, exploiting all its features. 

The convolutions were performed on $16 \times 16$, $32 \times 32$, and $64 \times 64$ input images with filter kernels of size between $3 \times 3$ and $7 \times 7$. The first set of experiments assesses speedup over a naive scalar single-thread implementation and estimate the performance boost of the \textbf{SIMD} paradigm. Figure~\ref{fig:results_scalarvssimd} depicts the results. By sweeping the size of the input image from $16 \times 16$ to $64 \times 64$, we observe a great increase in the effective acceleration (up to $5 \times$). This is due to the higher number of multiply-add operations that need to be performed. Unsurprisingly, small input images with filter kernels of size $7 \times 7$ have a reduced speedup due to the unbalanced sizes of the images and the filter causing a suboptimal use of the hardware lanes. 
%

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.70\textwidth]{Chapters/SPM/PRIME/figures/results_scalarvssimd.eps}
	\caption{Speedup over naive scalar single-thread implementation on $16 \times 16$, $32 \times 32$, and $64 \times 64$ input images with $3 \times 3$, $5 \times 5$, and $7 \times 7$ filter kernels.}
	\label{fig:results_scalarvssimd}
\end{figure}

Then, in a second set of experiments the benefits of using \textbf{multi-threading} are evaluated. Figure~\ref{fig:results_multithreading} shows the speedup over a single-thread implementation. Two threads ensure a speedup between $1.3 \times$ and $2 \times$, while a higher number of threads leads to better performance (up to $3.5 \times$). However, this trend is not constant since in case of small images and/or small filters, a higher number of threads may be useless. This is because hardware multi-threading involves some overhead for handling the different stacks (one for each thread) and for thread scheduling and synchronization. For instance, in case of a $16 \times 16$ input image and filters with a size of $3 \times 3$ and $7 \times 7$, the optimum number of threads is respectively two and six. This is because convolutions performed on a $16 \times 16$ input image with filter kernels of size $7 \times 7$ require $2.6$ more arithmetic operations than in case of $3 \times 3$ filters.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.65\textwidth]{Chapters/SPM/PRIME/figures/results_multithreading.eps}
	\caption{Speedup over single-thread implementation when varying the number of threads on $16 \times 16$, $32 \times 32$, and $64 \times 64$ input images with $3 \times 3$, $5 \times 5$, and $7 \times 7$ filter kernels.}
	\label{fig:results_multithreading}
\end{figure}

Finally, in the last set of experiments explore the benefits of using a \textbf{non-coherent scratchpad memory}. The results in terms of speedup over an accelerator with a standard memory subsystem are summarized in Figure~\ref{fig:results_scratchpad}. 

In case of smaller filters, we observe better results since there is a lower need to swap data between the scratchpad and the main memory. In general, the achieved speedup scales up with the number of threads up to $1.75 \times$. This is due to the higher efficiency of the scratchpad memory, which does not implement the coherence functionalities of traditional cache memories, as well as the lower number of cache misses when using the scratchpad memory (up to $30 \%$).
%
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.65\textwidth]{Chapters/SPM/PRIME/figures/results_scratchpad.eps}
	\caption{The speedup achieved using scratchpad memory when varying the number of threads on $16 \times 16$, $32 \times 32$, and $64 \times 64$ input images with $3 \times 3$, $5 \times 5$, and $7 \times 7$ filter kernels.}
	\label{fig:results_scratchpad}
\end{figure}

\section{Conclusions} \label{conclusions-sec}
\input{Chapters/SPM/PRIME/Conclusion/conclusion.tex}

%\bibliographystyle{IEEEtran}
%\bibliography{bibliography}