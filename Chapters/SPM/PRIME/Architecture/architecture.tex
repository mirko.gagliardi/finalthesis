%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%%    		Overview 
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  1 - Multithreaded cores
%%  2 - SIMD support

\begin{figure*}[ht]
	\centering
	\includegraphics[width=0.75\textwidth]{Chapters/SPM/PRIME/figures/nuplus_core.eps}
	\caption{Simplified overview of the developed GPU-like accelerator. This figure highlights both data and thread level parallelism. Beside memory configuration parameters, this architecture has different customization levels, such as the number of hardware lanes and the number of hardware threads implemented.}
	\label{fig:nuplus_architecture}
\end{figure*}

This work relies on an experimental platform, called nu+, providing a parameterizable GPU-like architecture inspired by modern GPUs, yet exposing full customization capabilities for architectural exploration. The heart of the platform is a RISC in-order core oriented to highly data-parallel kernels with a lightweight control infrastructure, shown in Figure \ref{fig:nuplus_architecture}. Most of its resources are dedicated to computation-intensive operations on massive datasets. Such accelerator blends together a hardware multithreading paradigm with a vector processor model. Each hardware thread has private internal resources such as PC, register file, and control registers along with a private memory stack, although all threads share the same compute units, L1 cache and an on-chip non-coherent memory. The thread control unit implements an interleaved multithreading scheduling in a fine-grain way. An internal round robin arbiter issues instructions for different threads in a fair mode after every cycle with a low architectural impact. Execution datapaths and register files are designed to exploit data-level parallelism. The architecture implements an instruction set containing instructions that operate on arrays of data. Computational units are organized in hardware vector lanes, with each scalar operator being instantiated $N$ times. Dually, each thread is equipped with a vectorial register file, where each register can store up to $N$ scalar data in order to satisfy the execution pipeline data throughput. Such a data parallelism allows each thread to perform SIMD operations on $N$ independent data simultaneously. 
%In the default configuration, the nu+ core has $N=16$ hardware lane, each SIMD operation performs computation on $16$ 32-bit data. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%%    		nu+ Memory Subsystem
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  1 - Coherence mechanism
%%  2 - SPM: Concepts, motivations and drawbacks
%%  	a - Structure
%%  	b - Stalls

The proposed GPU-like architecture has an $n$-way set-associative write-back L1 cache strictly coupled with a light cache controller which implements a simple valid/invalid coherence mechanism. Such cache controller handles misses and memory transactions and it also provides both request serialization and merging mechanisms in order to correctly manage concurrent requests from different threads. The cache line width matches the internal hardware lanes capability, thus a read memory request loads $N$ scalar data from main memory and stores them into a vectorial register at once, minimizing requests and exploiting the internal parallelism of the GPU-like accelerator. 

As in general purpose platforms, the performance of custom accelerators is also critically dependent on data movement and memory accesses. In that respect, hardware coherence mechanisms introduce both architectural and data management overheads, which are not always necessary in some applications. Many modern parallel architectures utilize fast non-coherent on-chip memories, called {\em scratchpad memories} (SPMs). Since NVIDIA Fermi family, GPUs are equipped with this kind of memories, that are intensively used to facilitate communication across threads and to store partial outputs or temporary data that are not requested to be synchronized back into main memory. 

The nu+ core supports such kind of high-throughput non-coherent scratchpad memory, which is divided in a parameterized number of banks based on a user-configurable mapping function in order to support multiple memory accesses. The scratchpad memory is organized in multiple independently-accessible memory banks providing a high data access parallelism. Therefore, if all memory accesses request data mapped to different banks, they can be handled in parallel. However, when multiple requests are made for data within the same bank, conflicts occur and a resolution logic handles and serializes each request resulting in a significantly performance loss. In fact, whenever an $n$-way conflict is detected, such a serialization logic notifies to the GPU-like accelerator control logic that the memory is not able to receive any further request, then it splits the conflicting requests into $n$ conflict-free sub-requests issued serially in the next $n$ cycles. 
%In the default configuration the scratchpad request parallelism is congruent to the core hardware lanes number.

%The nu+ scratchpad memory relies on memory controller to resolve bank collisions at run-time ensuring a correct execution of scratchpad accesses from concurrent threads. Main component of such controller is a serialization logic unit which performs conflict detection and request serialization whenever a collision is detected, the resolution semantics is the same described above for state-of-the-art scratchpad memories. Whenever an n-way conflict is detected, such a serialization logic signals to the GPU-like accelerator control logic that the memory is not able to receive any further request, then splits the conflicting requests into n conflict-free sub-requests issued serially in the next n cycles. When the last request is issued, the serialization logic signals back to the core control unit that the scratchpad is available again. On the other hand, multiple accesses to the same address are not seen as a conflict are not resulting in a bank conflict, the output is broadcasted to all hardware lanes. Furthermore, this memory has been provided with a dynamic address mapping mechanism in order to minimize bank collisions. Such a paradigm changes the relationship between memory addresses and internal banks at run-time. This is a key feature in that it allows the adoption of different mapping strategies that best suit the current workload.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%%    		nu+ toolchain
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The implemented architecture comes with a toolchain based on the LLVM project and includes a custom version of the Clang frontend and a native nu+ backend. The Clang front-end allows users to compile traditional C/C++ source code in a fast way and with a low memory usage. On the other hand, the toolchain is deeply customized for exploiting the core internal data parallelism and reaching the maximum throughput. The compiler has a complete vision of the SIMD nature of the datapath. It supports custom vector types, thus standard arithmetic and bitwise operators are available for both scalar and vector operations. Furthermore, the custom version of Clang supports ad-hoc builtin functions that are required to fully exploit target specific features, such as thread synchronization and special SIMD operations.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%%    		nu+ conclusion
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Combining a non-coherent on-chip memory approach, high data parallelism, and a fine-grain thread control, this GPU-like accelerator provides a significant speed-up in compute-intensive and  data demanding workloads.  
